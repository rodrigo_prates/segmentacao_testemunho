import csv
import os
import numpy as np
import cv2
import shutil
import imutils
import psutil
import random
import sys

from keras.preprocessing.image import load_img, save_img, ImageDataGenerator
from sklearn.model_selection import train_test_split
from keras.layers import Input
from keras.models import Model, load_model
from sklearn.metrics import classification_report, confusion_matrix
from keras import backend as K

import multiprocessing as mp

from keras_segmentation.predict import predict, predict_multiple, model_from_checkpoint_path
from keras_segmentation.data_utils.data_loader import *
from keras_segmentation.models.config import IMAGE_ORDERING
from keras_segmentation import metrics

from datetime import datetime

from tqdm import tqdm

from PostProcessImages import PostProcessImages

import concurrent.futures

from multiprocessing import cpu_count

import math


random.seed(0)
class_colors = [  ( random.randint(0,255),random.randint(0,255),random.randint(0,255)   ) for _ in range(5000)  ]


class ImageUtils:

   def __init__(self, debug, logfolder, logfile):
      self.debug = True if debug == 1 else False
      self.logfolder = logfolder
      if not os.path.exists(logfolder):
         os.makedirs(logfolder)
      self.logfile = logfile  

   def readimage(self, file):
      try:
         img = load_img(file)
      except IOError as e:
         print(e)

   def write_to_csv(self, csvdata, file, header, sep):
      with open(file, 'w', newline='') as csvfile:
         csvwriter = csv.writer(csvfile, delimiter=sep, quotechar='|', quoting=csv.QUOTE_MINIMAL)
         csvwriter.writerow(header)
         for row in csvdata:
            csvwriter.writerow(row)

   def write_dict_to_csv(self, csvdata, folder):
      file = os.path.sep.join([folder, 'labelshist.csv'])
      with open(file, 'w') as csvfile:
         csvfile.write("%s,%s\n"%('label','occurrences'))
         for key in csvdata.keys():
            csvfile.write("%s,%s\n"%(key,csvdata[key]))

   def write_lists_to_csv(self, outfolder, slidenames_list, labels_list, sep, header, filename):
      csvdata = [ [image, label] for image, label in zip(slidenames_list, labels_list) ]
      file = os.path.sep.join([outfolder, filename])
      self.write_to_csv(csvdata, file, header, sep)

   def dataseparation(self, imagenames, trainsize=75):
      numtrainimages = round(len(imagenames)*trainsize/100)
      trainimages = random.sample(imagenames, numtrainimages)
      testimages = [image for image in imagenames if image not in trainimages]
      return trainimages, testimages

   def datasplitting(self, input_list, labels_list, trainsize=75):
      test_size = 1 - trainsize/100
      input_train, input_test, label_train, label_test = train_test_split(input_list, labels_list, test_size=test_size)
      input_test, input_val, label_test, label_val = train_test_split(input_test, label_test, test_size=0.5)

      return input_train, input_test, input_val, label_train, label_test, label_val

   def datasplittingfolder(self, corefolder, labelfolder, trainsize=75):
      self.write2log(self.logfile, 'start Splitting folder files {} - {}'.format(corefolder, datetime.today().strftime('%H:%M:%S')))
      test_size = 1 - trainsize/100
      core_win_list = []
      label_win_list = []
      for file in sorted(os.listdir(labelfolder)):
         #core_win_list.append(file.replace('_label', ''))
         core_win_list.append(file)
         label_win_list.append(file)

      self.write2log(self.logfile, 'Finished Splitting files - {}'.format(datetime.today().strftime('%H:%M:%S')))
      return self.datasplitting(core_win_list, label_win_list, trainsize)

   def readconfigfile(self, file):
      return infolder, outfolder, windowsize, mode, stepsize, offset, augment, sep

   # def organizedataset(self, settype, infolder, outfolder, files):
   #    setpath = os.path.sep.join([outfolder, settype])
   #    if os.path.exists(setpath):
   #       shutil.rmtree(setpath)
         
   #    for file in files:
   #       label = file.split('.')[0].split('_')[-1:][0]
   #       finalpath = os.path.sep.join([setpath, label])
   #       if not os.path.exists(finalpath):
   #          os.makedirs(finalpath)

   #       newimgpath = os.path.sep.join([finalpath, file])
   #       origimgpath = os.path.sep.join([infolder, file])
   #       shutil.copy2(origimgpath, newimgpath)

   #    return setpath

   def organizedataset(self, settype, originputfolder, origlabelfolder, outfolder, infiles, outfiles):
      self.write2log(self.logfile, 'start dataset folders organization {} - {}'.format(settype, datetime.today().strftime('%H:%M:%S')))
      input_settype = os.path.sep.join([outfolder, settype+'_input'])
      output_settype = os.path.sep.join([outfolder, settype+'_output'])

      try:
         if os.path.isdir(input_settype) and os.listdir(input_settype):
            shutil.rmtree(input_settype)

         if not os.path.isdir(input_settype):
            os.makedirs(input_settype)

         if os.path.isdir(output_settype)  and os.listdir(output_settype):
            shutil.rmtree(output_settype)

         if not os.path.isdir(output_settype):
            os.makedirs(output_settype)

         totalfiles = len(infiles)
         counter = 0
         for file in infiles:
            print("Copy input files process: {} %".format(round((100/totalfiles)*counter,2)))
            newfilepath = os.path.sep.join([input_settype, file])
            origfilepath = os.path.sep.join([originputfolder, file])
            shutil.copy2(origfilepath, newfilepath)
            counter += 1
         
         totalfiles = len(outfiles)
         counter = 0
         for file in outfiles:
            print("Copy output files process: {} %".format(round((100/totalfiles)*counter,2)))
            newfilepath = os.path.sep.join([output_settype, file])
            origfilepath = os.path.sep.join([origlabelfolder, file])
            shutil.copy2(origfilepath, newfilepath)
            counter += 1

      except OSError as e:
         self.write2log(self.logfile, str(e) + ' - {}\n'.format(datetime.today().strftime('%H:%M:%S')))
         print(e)

      self.write2log(self.logfile, 'folders organization {} done - {}'.format(settype, datetime.today().strftime('%H:%M:%S')))
      return input_settype, output_settype          

   def removeimages(self, folder):
      for file in sorted(os.listdir(folder)):
         if file.endswith('.png'):
            os.remove(os.path.sep.join([folder, file]))

   def getneighbors(self, imglistnames, imgname, numneighbors):
      imageidx = imglistnames.index(imgname)
      maxidx = len(imglistnames)-1
      neighboridxs = []

      for idx in range(1, numneighbors+1):
         lidx = imageidx - idx
         hidx = imageidx + idx
         if lidx >= 0 and hidx <= maxidx:
            neighboridxs.append(lidx)
            neighboridxs.append(hidx)

      return [imglistnames[x] for x in neighboridxs]

   def removeimagesfromlist(self, imglistnames, imglistlabels, imgs2remove):
      for idx in range(0, len(imgs2remove)):
         img = imgs2remove[idx]
         if img in imglistnames:
            imglistnames.remove(img)
            imglistlabels.pop(idx)

      return imglistnames, imglistlabels

   def calcbatchsize(self):
      vm = psutil.virtual_memory()
      batchsize = 32

      if vm.available > 16e9 and vm.available <= 32e9:
         batchsize = batchsize*2
      elif vm.available > 32e9 and vm.available <= 64e9:
         batchsize = batchsize*4
      elif vm.available > 64e9:
         batchsize = batchsize*8

      return batchsize

   def getcorelabels(self, labelfile, sep):
      with open(labelfile, newline='') as csvfile:
         csvreader = csv.reader(csvfile, delimiter=sep, quotechar='|')
         return [row for row in csvreader if not row[0] == 'prof']

   def getlabelcolors(self, labels):
      numlabels = len(labels)
      labelcolordict = {}
      r = int(random.random() * 256)
      g = int(random.random() * 256)
      b = int(random.random() * 256)

      step = 256 / numlabels
      for label in labels:
         r += step
         g += step
         b += step
         r = int(r) % 256
         g = int(g) % 256
         b = int(b) % 256
         labelcolordict[label] = (r,g,b)

      return labelcolordict

   # def gen_windowed_images(self, image, imagename, core_labels, outfolder, windowsize=[180, 180], mode='vertical', stepsize=60, offset=2, augment = False, img_extension='png', pixel_length=3e-05):
   #    voffset = round(offset*image.size[1]/100)
   #    hoffset = round(offset*image.size[0]/100)
   #    slidenames = []
   #    labels = []
   #    if mode == 'vertical':
   #       imgmiddle = image.size[0]/2
   #       winleftpos = round(imgmiddle - windowsize[0]/2)
   #       winrightpos = round(imgmiddle + windowsize[0]/2)
   #       for vindex in range(voffset, image.size[1] - windowsize[1] - voffset, stepsize):
   #          imgwindow = image.crop(( winleftpos, vindex, winrightpos, vindex + windowsize[1] ))
   #          prof = vindex*pixel_length
   #          label = self.get_label(prof, core_labels)
   #          slidename = imagename + '_' + str(vindex) + '_' + label.strip() + '.' + img_extension
   #          slidenames.append(slidename)
   #          labels.append(label)
   #          save_img(os.path.sep.join([outfolder, slidename]), imgwindow)

   #          if augment:
   #             slidename = imagename + 'f_' + str(vindex) + '_' + label.strip() + '.' + img_extension
   #             slidenames.append(slidename)
   #             labels.append(label)
   #             imgwindow = PreProcessImages.flipimage(imgwindow)
   #             save_img(os.path.sep.join([outfolder, slidename]), imgwindow)

   #          if self.debug:
   #             print(slidename)

   #    else:
   #       label = '1'
   #       for vindex in range(voffset, image.size[1] - windowsize[1] - voffset, stepsize):
   #          for hindex in range(hoffset, image.size[0] - windowsize[0] - hoffset, stepsize):
   #             imgwindow = image.crop(( hindex, vindex, hindex + windowsize[0], vindex + windowsize[1] ))
   #             save_img(imagename + '_' + str(vindex) + '_' + str(hindex) + '_C' + label + '.png', imgwindow)

   #    return slidenames, labels

   def corepaint(self, imgfile, labelfile, sep, outfolder, windowsize, mode, stepsize, offset, meansub, modelfolder, targetsize, labels, colorsdict, threshold, debug, img_extension, modelname, pixel_length):
      img = load_img(imgfile)
      imagefilename = os.path.basename(imgfile)
      core_labels = UtilsImages.getcorelabels(labelfile, sep)
      #segimages = SegImages(debug)
      slidenames, slidelabels = self.gen_windowed_images(img, imagefilename.split('.')[0], core_labels, outfolder, windowsize, mode, stepsize, offset, False, img_extension, pixel_length)

      print("[INFO] loading model...")
      model = load_model(modelfolder)

      predsegimage = np.ones(shape=[img.size[1], img.size[0], 3], dtype=np.uint8)
      origsegimage = np.ones(shape=[img.size[1], img.size[0], 3], dtype=np.uint8)
      
      voffset = round(offset*img.size[1]/100)
      predictlabels = []
      for idx in range(0, len(slidenames)):
         print('predict window image: ')
         print(idx)
         image = cv2.imread(os.path.sep.join([outfolder, slidenames[idx]]))
         output = image.copy()
         output = imutils.resize(output, width=400)
         image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
         image = cv2.resize(image, targetsize)
         image = image.astype("float32")
         mean = meansub
         image -= mean

         preds = model.predict(np.expand_dims(image, axis=0))[0]

         i = np.argmax(preds)
         predictlabels.append(slidelabels[i])

         if preds[i] > threshold:

            vertop = int(slidenames[idx].split('_')[-2]) + voffset
            verdown = vertop + windowsize[1]
            origlabel = slidenames[idx].split('.')[0].split('_')[-1]

            predsegimage[ vertop:verdown, : ] = colorsdict[slidelabels[i]]
            origsegimage[ vertop:verdown, : ] = colorsdict[origlabel]

            os.remove(os.path.sep.join([outfolder, slidenames[idx]]))

      cv2.imwrite(os.path.sep.join([ outfolder, modelname, 'predseg_' + imagefilename ]), predsegimage)
      cv2.imwrite(os.path.sep.join([ outfolder, 'origseg_' + imagefilename ]), origsegimage)

      self.write_lists_to_csv(outfolder, slidelabels, predictlabels, sep, ['classes', 'predicoes'], 'predict.csv')

      return predictlabels, slidelabels

   def encode_labels(self, labels):
      labels_encoded = list(range(0, len(labels)))
      return dict(zip(labels, labels_encoded))

   def load_features(self, features_path):
      data = []
      labels = []

      for row in open(features_path):
         row = row.strip().split(',')
         label = row[0]
         features = np.array(row[1:], dtype='float')

         data.append(features)
         labels.append(label)
 
      data = np.array(data)
      labels = np.array(labels)
 
      return (data, labels)

   def bin2format(self, outformat, bin_file_path, height, width, output_file_path, labels):

      print("Convert image {}".format(bin_file_path))

      bin_file = open(bin_file_path, "r")
      bin_list = np.fromfile(bin_file, dtype=np.uint8)
      bin_file.close()

      bin_list = bin_list.reshape(height, width, 4)

      formatted_image = np.zeros(shape=[height, width, 3], dtype=np.uint8)

      img_color = (0, 0, 0)
      for h in range(height):
         if h % 50 == 0:
            print("Image convertion rate: {} %".format(round((100/height)*h, 2)))
            print("image_color {}".format(img_color))
         for w in range (width):
            color_arr = bin_list[h,w]
            img_color = (color_arr[0], color_arr[1], color_arr[2])
            if outformat == 'segmented_image':
               formatted_image[h,w] = img_color
            else:
               formatted_image[h,w] = labels[img_color]

      print('save image: {}'.format(output_file_path))
      try:
         if outformat == 'segmented_image':
            cv2.imwrite(output_file_path, cv2.cvtColor(formatted_image, cv2.COLOR_RGB2BGR))
         else:
            cv2.imwrite(output_file_path, formatted_image)
      except cv2.error as e:
         print(e)

      return formatted_image

   def paintpredicted(self, imgfile, labels, modelpath, windowsize, stepsize, mode, offset, outfolder, modelname, modelfilepath):

      self.write2log(self.logfile, 'start paint prediction {} - {}'.format(imgfile, datetime.today().strftime('%H:%M:%S')))

      resultpath = os.path.sep.join([outfolder, modelname])

      if not os.path.exists(resultpath):
         os.makedirs(resultpath)
      
      img = cv2.imread(imgfile)
      imagefilename = os.path.basename(imgfile)
      height, width = img.shape[:2]
      voffset = round(offset*height/100)
      hoffset = round(offset*width/100)

      print("[INFO] loading model...")
      model = model_from_checkpoint_path(modelpath, modelfilepath)
      predsegimage = np.zeros(shape=[height, width, 3], dtype=np.uint8)

      output_width = model.output_width
      output_height  = model.output_height
      input_width = model.input_width
      input_height = model.input_height
      n_classes = model.n_classes

      try:

         if mode == 'horizontal':
            for vindex in range(voffset, height - windowsize[1] - voffset, stepsize):
               vert = height - windowsize[1] - voffset
               print("Image windowing process: {}%".format(round((100/(vert))*vindex,2)))
               for hindex in range(hoffset, width - windowsize[0] - hoffset, stepsize):
                  imgwindow = img[vindex:vindex + windowsize[1], hindex:hindex + windowsize[0]]
                  imgwindow_arr = get_image_arr(imgwindow, input_width, input_height, odering=IMAGE_ORDERING)
                  pr = model.predict(np.array([imgwindow_arr]))[0]
                  pr = pr.reshape((output_height, output_width, n_classes)).argmax( axis=2 )

                  seg_img = np.zeros((output_height, output_width, 3))
                  colors = class_colors

                  for c in range(n_classes):
                     color = list(labels.keys())[list(labels.values()).index(c)]
                     seg_img[:,:,0] += ( (pr[:,: ] == c )*( color[2] )).astype('uint8')
                     seg_img[:,:,1] += ((pr[:,: ] == c )*( color[1] )).astype('uint8')
                     seg_img[:,:,2] += ((pr[:,: ] == c )*( color[0] )).astype('uint8')
                  #seg_img = cv2.resize(seg_img, (input_width, input_height))
                  seg_img = cv2.resize(seg_img, (windowsize[0], windowsize[1]))

                  predsegimage[vindex:vindex + windowsize[1], hindex:hindex + windowsize[0]] = seg_img

            cv2.imwrite(os.path.sep.join([ resultpath, 'predseg_' + imagefilename ]), predsegimage)

         self.write2log(self.logfile, 'End paint prediction {} - {}'.format(imgfile, datetime.today().strftime('%H:%M:%S')))
         return predsegimage

      except Exception as e:
         self.write2log(self.logfile, str(e) + ' - {}\n'.format(datetime.today().strftime('%H:%M:%S')))
         print(e)


   def evaluate(self, model, input_imgfile, annotation_imgfile, windowsize, stepsize, mode, 
      offset, outfolder, modelname, num_labels, label_names, imgNorm, conf_matrix_file_name):

      self.write2log(self.logfile, 'start evaluation of {} - {}'.format(input_imgfile, datetime.today().strftime('%H:%M:%S')))

      resultpath = os.path.sep.join([outfolder, modelname])
      if not os.path.exists(resultpath):
         os.makedirs(resultpath)

      img = cv2.imread(input_imgfile)
      #imagefilename = os.path.basename(input_imgfile)
      img_height, img_width = img.shape[:2]
      img_voffset = round(offset*img_height/100)
      img_hoffset = round(offset*img_width/100)

      ann = cv2.imread(annotation_imgfile)
      #imagefilename = os.path.basename(annotation_imgfile)
      ann_height, ann_width = ann.shape[:2]
      ann_voffset = round(offset*ann_height/100)
      ann_hoffset = round(offset*ann_width/100)

      output_width = model.output_width
      output_height  = model.output_height
      input_width = model.input_width
      input_height = model.input_height
      n_classes = model.n_classes

      #expected = np.zeros(num_labels, dtype=np.uint8)
      conf_matrix = np.zeros((num_labels, num_labels))

      total_iou = 0
      classes_iou_list = []
      expected_list = []
      #count = 0
      try:
         final_ious = []
         if mode == 'horizontal':
            for vindex in range(img_voffset, img_height - windowsize[1] - img_voffset, stepsize):
               vert = img_height - windowsize[1] - img_voffset
               print("Image windowing for evaluation process: {}%".format(round((100/(vert))*vindex,2)))
               #count = count + 1
               #if count > 5:
               #   break
               for hindex in range(img_hoffset, img_width - windowsize[0] - img_hoffset, stepsize):

                  imgwindow = img[vindex:vindex + windowsize[1], hindex:hindex + windowsize[0]]
                  annwindow = ann[vindex:vindex + windowsize[1], hindex:hindex + windowsize[0]]
                  if imgNorm == "sub_mean_no_resize":
                     input_width = windowsize[0]
                     input_height = windowsize[1]
                     output_width = input_width/2
                     output_height = input_height/2
                  imgwindow_arr = get_image_arr(imgwindow, input_width, input_height, odering=IMAGE_ORDERING)
                  pr = model.predict(np.array([imgwindow_arr]))[0]
                  pr = pr.reshape((output_height, output_width, n_classes)).argmax( axis=2 )
                  pr = pr.reshape((output_width*output_height))
                  
                  gt = get_segmentation_arr(annwindow, n_classes, output_width, output_height)
                  gt = gt.argmax(-1)
                  conf_matrix = conf_matrix + confusion_matrix(gt, pr, list(range(0, num_labels)))
                  #conf_matrix = conf_matrix + confusion_matrix(gt, pr, label_names)
                  expected = np.zeros(num_labels, dtype=np.uint8)
                  for v in gt:
                     expected[v] = 1 
                  classes_iou = metrics.get_iou(gt, pr, n_classes)
                  classes_iou_list.append(classes_iou)
                  expected_list.append(expected)
                  final_iou = np.mean( np.take(classes_iou, np.nonzero(expected)) )
                  final_ious.append( final_iou )

            classes_iou_list = np.array( classes_iou_list )
            expected_list = np.array( expected_list )
            #final_classes_iou = np.true_divide(classes_iou_list.sum(0),(classes_iou_list!=0).sum(0))
            #final_classes_iou = np.nan_to_num(final_classes_iou, 0)
            final_classes_iou, std_classes_iou = self.columnsMeanStd(classes_iou_list, expected_list)
            final_ious = np.array( final_ious )
            total_iou = np.mean(final_ious, axis=0)
            std_iou = np.std(final_ious)
            conf_matrix = conf_matrix / np.max(np.abs(conf_matrix))
            print("Total IoU ", total_iou)
            print("Std Iou", std_iou)
            print("Total IoU for each class", final_classes_iou)
            print("Std IoU for each class", std_classes_iou)
            #print("confusion matrix", conf_matrix)

            self.write2log( os.path.sep.join([resultpath, 'result.txt']), "File: " + input_imgfile )
            data2print = list(zip(final_classes_iou, std_classes_iou))
            self.write2log( os.path.sep.join([resultpath, 'result.txt']), "Total Classes IoU: " + self.printTupleList(data2print, " +/- ") )
            self.write2log( os.path.sep.join([resultpath, 'result.txt']), "Total IoU: " + str(total_iou) + " +/- " + str(std_iou) )
            self.write2log( os.path.sep.join([resultpath, 'result.txt']), "Confusion Matrix: " + str(conf_matrix) )

            #conf_matrix = 255 * conf_matrix
            sum_of_rows = conf_matrix.sum(axis=1)
            conf_matrix_normalized = conf_matrix / sum_of_rows[:, np.newaxis]
            conf_matrix_normalized = np.nan_to_num(conf_matrix_normalized, 0)
            print("confusion matrix", conf_matrix_normalized)
            postprocess = PostProcessImages(True, self.logfolder, self.logfile)
            plotpath = os.path.sep.join([ resultpath, conf_matrix_file_name ])
            postprocess.plotConfMatrix(conf_matrix_normalized, label_names, plotpath)
            #cv2.imwrite(os.path.sep.join([ resultpath, 'confusion_matrix.png' ]), conf_matrix.astype(int))

            self.write2log(self.logfile, 'End evaluation of {} - {}'.format(input_imgfile, datetime.today().strftime('%H:%M:%S')))

         return total_iou

      except Exception as e:
         exc_type, exc_obj, exc_tb = sys.exc_info()
         fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
         print(exc_type, fname, exc_tb.tb_lineno)
         self.write2log(self.logfile, str(e) + ' - {}\n'.format(datetime.today().strftime('%H:%M:%S')))
         print(e)

   def columnsMeanStd(self, nd_array, expected_list):
      col_means = []
      col_stds = []
      for col_idx in range(0, nd_array.shape[1]):
         col_values = np.take(nd_array[:,col_idx], np.nonzero(expected_list[:,col_idx]))
         col_means.append(np.mean(col_values))
         col_stds.append(np.std(col_values))

      return np.nan_to_num(col_means, 0), np.nan_to_num(col_stds, 0)

   def printTupleList(self, tuple_list, concat_str):
      result_str = ''
      for element in tuple_list:
         result_str = result_str + str(element[0]) + concat_str + str(element[1])

      return result_str

   def write2log(self, logfile, log):
      txtfile = open(logfile, "a+")
      txtfile.write(log + '\n')
      txtfile.close()

   def check_dataset_balance(self, labels, folder):
      num_labels = len(labels)
      colors_freq = np.zeros(num_labels, dtype=np.uint32)
      for imgfile in sorted(os.listdir(folder)):
         imgfile = os.path.sep.join([folder, imgfile])
         print('reading image: {}'.format(imgfile))
         img = cv2.imread(imgfile)
         colors_freq += self.check_labels_balance(labels, num_labels, img)
         print(colors_freq)

      self.write2log('dataset_balance.txt', str(colors_freq))

      return colors_freq

   def check_labels_balance(self, labels, num_labels, image):
      height, width = image.shape[:2]
      colors_freq = np.zeros(num_labels, dtype=np.uint32)
      for h in range(height):
         for w in range(width):
            color_id = image[h,w][0]
            colors_freq[ color_id ] += 1

      return colors_freq

   def lower_case_ext(self, folder):
      for file in sorted(os.listdir(folder)):
         newfile = os.path.splitext(file)[0] + os.path.splitext(file)[1].lower()
         os.rename( os.path.sep.join([folder, file]), os.path.sep.join([folder, newfile]) )

   def gen_prediction_from_folder(self, model, input_imgfile, annotation_imgfile, outfolder, modelname, 
   num_labels, label_names, conf_matrix_file_name):
      
      self.write2log(self.logfile, 'start gen_prediction_from_folder of {} - {}'.format(input_imgfile, datetime.today().strftime('%H:%M:%S')))

      resultpath = os.path.sep.join([outfolder, modelname])
      if not os.path.exists(resultpath):
         os.makedirs(resultpath)

      output_width = model.output_width
      output_height  = model.output_height
      input_width = model.input_width
      input_height = model.input_height
      n_classes = model.n_classes
      #input_images = []
      output_images = []
      conf_matrix = np.zeros((num_labels, num_labels))
      classes_iou_list = []
      expected_list = []
      final_ious = []

      test_gen = ImageDataGenerator()
      test_flow = test_gen.flow_from_directory(input_imgfile, class_mode=None, classes=None, 
      target_size=(input_width,input_height), batch_size=32, shuffle=False)

      test_flow.reset()
      prs=model.predict_generator(test_flow,verbose=1)
      input_images = test_flow.filenames
      count = 0
      totalfiles = len(input_images)

      print('generate confusion matrix')
      for i in range(0, prs.shape[0]):
         pr = prs[i]
         ann = cv2.imread(os.path.sep.join([annotation_imgfile, input_images[i]]))
         pr = pr.reshape((output_height, output_width, n_classes)).argmax( axis=2 )
         pr = pr.reshape((output_width*output_height))
         gt = get_segmentation_arr(ann, n_classes, output_width, output_height)
         gt = gt.argmax(-1)
         conf_matrix = conf_matrix + confusion_matrix(gt, pr, list(range(0, num_labels)))
         expected = np.zeros(num_labels, dtype=np.uint8)
         for v in gt:
            expected[v] = 1
         classes_iou = metrics.get_iou(gt, pr, n_classes)
         classes_iou_list.append(classes_iou)
         expected_list.append(expected)
         final_iou = np.mean( np.take(classes_iou, np.nonzero(expected)) )
         final_ious.append( final_iou )
         count = count + 1
         print('Remain files: {}'.format(str(totalfiles-count)))

      classes_iou_list = np.array( classes_iou_list )
      expected_list = np.array( expected_list )
      final_classes_iou, std_classes_iou = self.columnsMeanStd(classes_iou_list, expected_list)
      final_ious = np.array( final_ious )
      total_iou = np.mean(final_ious, axis=0)
      std_iou = np.std(final_ious)
      conf_matrix = conf_matrix / np.max(np.abs(conf_matrix))
      print("Total IoU ", total_iou)
      print("Std Iou", std_iou)
      print("Total IoU for each class", final_classes_iou)
      print("Std IoU for each class", std_classes_iou)

      data2print = list(zip(final_classes_iou, std_classes_iou))
      self.write2log( os.path.sep.join([resultpath, 'predictions.txt']), "Total Classes IoU: " + self.printTupleList(data2print, " +/- ") )
      self.write2log( os.path.sep.join([resultpath, 'predictions.txt']), "Total IoU: " + str(total_iou) + " +/- " + str(std_iou) )
      self.write2log( os.path.sep.join([resultpath, 'predictions.txt']), "Confusion Matrix: " + str(conf_matrix) )

      sum_of_rows = conf_matrix.sum(axis=1)
      conf_matrix_normalized = conf_matrix / sum_of_rows[:, np.newaxis]
      conf_matrix_normalized = np.nan_to_num(conf_matrix_normalized, 0)
      print("confusion matrix", conf_matrix_normalized)
      postprocess = PostProcessImages(True, self.logfolder, self.logfile)
      plotpath = os.path.sep.join([ resultpath, conf_matrix_file_name ])
      postprocess.plotConfMatrix(conf_matrix_normalized, label_names, plotpath)

      self.write2log(self.logfile, 'End gen_prediction_from_folder for folder {} - {}'.format(input_imgfile, datetime.today().strftime('%H:%M:%S')))

   def batch_prediction_from_folder(self, model, input_imgfile, annotation_imgfile, outfolder, modelname, 
   num_labels, label_names, conf_matrix_file_name, max_images_to_test, num_cpus, auto_num_cpus):

      self.write2log(self.logfile, 'start batch_prediction_from_folder of {} - {}'.format(input_imgfile, datetime.today().strftime('%H:%M:%S')))
      
      resultpath = os.path.sep.join([outfolder, modelname])
      if not os.path.exists(resultpath):
         os.makedirs(resultpath)
      
      output_width = model.output_width
      output_height  = model.output_height
      input_width = model.input_width
      input_height = model.input_height
      n_classes = model.n_classes
      input_images = []
      output_images = []
      conf_matrix = np.zeros((num_labels, num_labels))
      classes_iou_list = []
      expected_list = []
      final_ious = []

      dirfiles = sorted(os.listdir(input_imgfile))
      if max_images_to_test:
         dirfiles = dirfiles[0:max_images_to_test]

      totalfiles = len(dirfiles)

      workers = num_cpus if not auto_num_cpus else cpu_count()
      workers = workers if len(dirfiles) >= workers else len(dirfiles)

      print('Generate batch list')
      for file in dirfiles:
         image = cv2.imread(os.path.sep.join([input_imgfile, file]))
         ann = cv2.imread(os.path.sep.join([annotation_imgfile, file]))
         img_arr = get_image_arr(image, input_width, input_height, odering=IMAGE_ORDERING)
         gt = get_segmentation_arr(ann, n_classes, output_width, output_height)
         gt = gt.argmax(-1)
         input_images.append(img_arr)
         output_images.append(gt)

      input_images = np.array(input_images)
      print('predict batch list with model {}'.format(modelname))
      prs = model.predict(input_images, batch_size=32, workers=workers)
      count = 0

      print('generate confusion matrix')
      for i in range(0, prs.shape[0]):
         pr = prs[i]
         gt = output_images[i]
         pr = pr.reshape((output_height, output_width, n_classes)).argmax( axis=2 )
         pr = pr.reshape((output_width*output_height))
         conf_matrix = conf_matrix + confusion_matrix(gt, pr, list(range(0, num_labels)))
         expected = np.zeros(num_labels, dtype=np.uint8)
         for v in gt:
            expected[v] = 1
         classes_iou = metrics.get_iou(gt, pr, n_classes)
         classes_iou_list.append(classes_iou)
         expected_list.append(expected)
         final_iou = np.mean( np.take(classes_iou, np.nonzero(expected)) )
         final_ious.append( final_iou )
         count = count + 1
         print('Remain files: {}'.format(str(totalfiles-count)))

      classes_iou_list = np.array( classes_iou_list )
      expected_list = np.array( expected_list )
      final_classes_iou, std_classes_iou = self.columnsMeanStd(classes_iou_list, expected_list)
      final_ious = np.array( final_ious )
      total_iou = np.mean(final_ious, axis=0)
      std_iou = np.std(final_ious)
      conf_matrix = conf_matrix / np.max(np.abs(conf_matrix))
      print("Total IoU ", total_iou)
      print("Std Iou", std_iou)
      print("Total IoU for each class", final_classes_iou)
      print("Std IoU for each class", std_classes_iou)

      data2print = list(zip(final_classes_iou, std_classes_iou))
      self.write2log( os.path.sep.join([resultpath, 'predictions.txt']), "Total Classes IoU: " + self.printTupleList(data2print, " +/- ") )
      self.write2log( os.path.sep.join([resultpath, 'predictions.txt']), "Total IoU: " + str(total_iou) + " +/- " + str(std_iou) )
      self.write2log( os.path.sep.join([resultpath, 'predictions.txt']), "Confusion Matrix: " + str(conf_matrix) )

      sum_of_rows = conf_matrix.sum(axis=1)
      conf_matrix_normalized = conf_matrix / sum_of_rows[:, np.newaxis]
      conf_matrix_normalized = np.nan_to_num(conf_matrix_normalized, 0)
      print("confusion matrix", conf_matrix_normalized)
      postprocess = PostProcessImages(True, self.logfolder, self.logfile)
      plotpath = os.path.sep.join([ resultpath, conf_matrix_file_name ])
      postprocess.plotConfMatrix(conf_matrix_normalized, label_names, plotpath)

      self.write2log(self.logfile, 'End batch_prediction_from_folder for folder {} - {}'.format(input_imgfile, datetime.today().strftime('%H:%M:%S')))

   def multiprocess_predict_from_folder(self, model, input_imgfile, annotation_imgfile, outfolder, modelname, 
   num_labels, label_names, conf_matrix_file_name, max_images_to_test, num_cpus, auto_num_cpus):
      
      self.write2log(self.logfile, 'start evaluation of {} - {}'.format(input_imgfile, datetime.today().strftime('%H:%M:%S')))

      resultpath = os.path.sep.join([outfolder, modelname])
      if not os.path.exists(resultpath):
         os.makedirs(resultpath)

      self.max_images_to_test = max_images_to_test
      self.input_imgfile = input_imgfile
      self.annotation_imgfile = annotation_imgfile
      self.model = model

      output_width = model.output_width
      output_height  = model.output_height
      input_width = model.input_width
      input_height = model.input_height
      n_classes = model.n_classes

      self.output_width = output_width
      self.output_height = output_height
      self.input_width = input_width
      self.input_height = input_height
      self.n_classes = n_classes
      self.num_labels = num_labels

      dirfiles = os.listdir(input_imgfile)
      totalfiles = len(dirfiles)

      if not max_images_to_test:
         max_images_to_test = 999999
      else:
         totalfiles = max_images_to_test

      total_iou = 0
      self.classes_iou_list = []
      self.expected_list = []
      self.conf_matrix = np.zeros((num_labels, num_labels))
      self.final_ious = []

      self.totalfiles = totalfiles

      workers = num_cpus if not auto_num_cpus else cpu_count()

      workers = workers if len(dirfiles) >= workers else len(dirfiles)
      
      chunk_list = self.chunk_data(sorted(dirfiles), workers)

      with concurrent.futures.ProcessPoolExecutor(max_workers=workers) as executor:
         execution_results = []
         for i in range(0, workers):
            execution_results.append(executor.submit(self.predict_sliding_images, chunk_list[i]))

         concurrent.futures.as_completed(execution_results)

      self.classes_iou_list = np.array( self.classes_iou_list )
      self.expected_list = np.array( self.expected_list )
      final_classes_iou, std_classes_iou = self.columnsMeanStd(self.classes_iou_list, self.expected_list)
      self.final_ious = np.array( self.final_ious )
      total_iou = np.mean(self.final_ious, axis=0)
      std_iou = np.std(self.final_ious)
      self.conf_matrix = self.conf_matrix / np.max(np.abs(self.conf_matrix))
      print("Total IoU ", total_iou)
      print("Std Iou", std_iou)
      print("Total IoU for each class", final_classes_iou)
      print("Std IoU for each class", std_classes_iou)

      data2print = list(zip(final_classes_iou, std_classes_iou))
      self.write2log( os.path.sep.join([resultpath, 'predictions.txt']), "Total Classes IoU: " + self.printTupleList(data2print, " +/- ") )
      self.write2log( os.path.sep.join([resultpath, 'predictions.txt']), "Total IoU: " + str(total_iou) + " +/- " + str(std_iou) )
      self.write2log( os.path.sep.join([resultpath, 'predictions.txt']), "Confusion Matrix: " + str(self.conf_matrix) )

      sum_of_rows = self.conf_matrix.sum(axis=1)
      conf_matrix_normalized = self.conf_matrix / sum_of_rows[:, np.newaxis]
      conf_matrix_normalized = np.nan_to_num(conf_matrix_normalized, 0)
      print("confusion matrix", conf_matrix_normalized)
      postprocess = PostProcessImages(True, self.logfolder, self.logfile)
      plotpath = os.path.sep.join([ resultpath, conf_matrix_file_name ])
      postprocess.plotConfMatrix(conf_matrix_normalized, label_names, plotpath)

      self.write2log(self.logfile, 'End predictions for folder {} - {}'.format(input_imgfile, datetime.today().strftime('%H:%M:%S')))

   def predict_sliding_images(self, files):
      count = 0
      for file in files:
         if count >= self.max_images_to_test:
            break
         print('predict file {}'.format(file))
         image = cv2.imread(os.path.sep.join([self.input_imgfile, file]))
         ann = cv2.imread(os.path.sep.join([self.annotation_imgfile, file]))
         img_arr = get_image_arr(image, self.input_width, self.input_height, odering=IMAGE_ORDERING)
         pr = self.model.predict(np.array([img_arr]))[0]
         pr = pr.reshape((self.output_height, self.output_width, self.n_classes)).argmax( axis=2 )
         pr = pr.reshape((self.output_width*self.output_height))
         gt = get_segmentation_arr(ann, self.n_classes, self.output_width, self.output_height)
         gt = gt.argmax(-1)
         self.conf_matrix = self.conf_matrix + confusion_matrix(gt, pr, list(range(0, self.num_labels)))
         expected = np.zeros(self.num_labels, dtype=np.uint8)
         for v in gt:
            expected[v] = 1 
         classes_iou = metrics.get_iou(gt, pr, self.n_classes)
         self.classes_iou_list.append(classes_iou)
         self.expected_list.append(expected)
         final_iou = np.mean( np.take(classes_iou, np.nonzero(expected)) )
         self.final_ious.append( final_iou )
         count = count + 1
         print('Remain files: {}'.format(str(self.totalfiles-count)))

      return count 

   def predict_from_folder(self, model, image_folder, annotation_folder, 
   outfolder, modelname, num_labels, label_names, max_images_to_test, conf_matrix_file_name):
      self.write2log(self.logfile, 'start predictions for folder {} - {}'.format(image_folder, datetime.today().strftime('%H:%M:%S')))
      
      resultpath = os.path.sep.join([outfolder, modelname])
      if not os.path.exists(resultpath):
         os.makedirs(resultpath)

      output_width = model.output_width
      output_height  = model.output_height
      input_width = model.input_width
      input_height = model.input_height
      n_classes = model.n_classes

      dirfiles = os.listdir(image_folder)
      totalfiles = len(dirfiles)
      count = 0

      if not max_images_to_test:
         max_images_to_test = 999999
      else:
         totalfiles = max_images_to_test

      total_iou = 0
      classes_iou_list = []
      expected_list = []
      conf_matrix = np.zeros((num_labels, num_labels))

      try:
         final_ious = []
         for file in sorted(dirfiles):
            if count >= max_images_to_test:
               break
            print('predict file {}'.format(file))
            image = cv2.imread(os.path.sep.join([image_folder, file]))
            ann = cv2.imread(os.path.sep.join([annotation_folder, file]))
            img_arr = get_image_arr(image, input_width, input_height, odering=IMAGE_ORDERING)
            pr = model.predict(np.array([img_arr]))[0]
            pr = pr.reshape((output_height, output_width, n_classes)).argmax( axis=2 )
            pr = pr.reshape((output_width*output_height))
            gt = get_segmentation_arr(ann, n_classes, output_width, output_height)
            gt = gt.argmax(-1)

            conf_matrix = conf_matrix + confusion_matrix(gt, pr, list(range(0, num_labels)))
            expected = np.zeros(num_labels, dtype=np.uint8)
            for v in gt:
               expected[v] = 1 
            
            classes_iou = metrics.get_iou(gt, pr, n_classes)
            classes_iou_list.append(classes_iou)
            expected_list.append(expected)
            final_iou = np.mean( np.take(classes_iou, np.nonzero(expected)) )
            final_ious.append( final_iou )
            count = count + 1
            print('Remain files: {}'.format(str(totalfiles-count)))

         classes_iou_list = np.array( classes_iou_list )
         expected_list = np.array( expected_list )
         final_classes_iou, std_classes_iou = self.columnsMeanStd(classes_iou_list, expected_list)
         final_ious = np.array( final_ious )
         total_iou = np.mean(final_ious, axis=0)
         std_iou = np.std(final_ious)
         conf_matrix = conf_matrix / np.max(np.abs(conf_matrix))
         print("Total IoU ", total_iou)
         print("Std Iou", std_iou)
         print("Total IoU for each class", final_classes_iou)
         print("Std IoU for each class", std_classes_iou)

         data2print = list(zip(final_classes_iou, std_classes_iou))
         self.write2log( os.path.sep.join([resultpath, 'predictions.txt']), "Total Classes IoU: " + self.printTupleList(data2print, " +/- ") )
         self.write2log( os.path.sep.join([resultpath, 'predictions.txt']), "Total IoU: " + str(total_iou) + " +/- " + str(std_iou) )
         self.write2log( os.path.sep.join([resultpath, 'predictions.txt']), "Confusion Matrix: " + str(conf_matrix) )

         sum_of_rows = conf_matrix.sum(axis=1)
         conf_matrix_normalized = conf_matrix / sum_of_rows[:, np.newaxis]
         conf_matrix_normalized = np.nan_to_num(conf_matrix_normalized, 0)
         print("confusion matrix", conf_matrix_normalized)
         postprocess = PostProcessImages(True, self.logfolder, self.logfile)
         plotpath = os.path.sep.join([ resultpath, conf_matrix_file_name ])
         postprocess.plotConfMatrix(conf_matrix_normalized, label_names, plotpath)

         self.write2log(self.logfile, 'End predictions for folder {} - {}'.format(image_folder, datetime.today().strftime('%H:%M:%S')))

      except Exception as e:

         self.write2log(self.logfile, str(e) + ' - {}\n'.format(datetime.today().strftime('%H:%M:%S')))
         print(e)
      

   def compare_images(self, imageA_path, imageB_path, resultpath):
      self.write2log(self.logfile, 'compare_images A: {} and B: {} at {}'.format(imageA_path, imageB_path, datetime.today().strftime('%H:%M:%S')))
      imgA = cv2.imread(imageA_path)
      imgB = cv2.imread(imageB_path)

      height, width = imgA.shape[:2]
      predsegimage = np.zeros(shape=[height, width, 3], dtype=np.uint8)
      white_color = 255*np.ones((3)).astype('uint8')

      try:
         for h in range(height):
            if h % 100 == 0:
               print('progress: {}%'.format( round((100/height)*h,2) ))
            for w in range(width):
               pA = imgA[h,w]
               #print('pA: {}'.format(str(pA)))
               pB = imgB[h,w]
               #print('pB: {}'.format(str(pB)))
               euclidian_dist = np.linalg.norm(pA.astype('float')-pB.astype('float'))
               #print('euclidian_dist: {}'.format(str(euclidian_dist)))
               if euclidian_dist > 0:
                  predsegimage[h,w] = white_color
      
         cv2.imwrite(os.path.sep.join([ resultpath, 'diff.png' ]), predsegimage)
      except cv2.error as e:
         self.write2log(self.logfile, str(e) + ' - {}\n'.format(datetime.today().strftime('%H:%M:%S')))
         print(e)

   def get_model_memory_usage(self, batch_size, model):

      shapes_mem_count = 0
      internal_model_mem_count = 0
      for l in model.layers:
         layer_type = l.__class__.__name__
         if layer_type == 'Model':
            internal_model_mem_count += get_model_memory_usage(batch_size, l)
         single_layer_mem = 1
         for s in l.output_shape:
            if s is None:
               continue
            single_layer_mem *= s
         shapes_mem_count += single_layer_mem

      trainable_count = np.sum([K.count_params(p) for p in set(model.trainable_weights)])
      non_trainable_count = np.sum([K.count_params(p) for p in set(model.non_trainable_weights)])

      number_size = 4.0
      if K.floatx() == 'float16':
         number_size = 2.0
      if K.floatx() == 'float64':
         number_size = 8.0

      total_memory = number_size*(batch_size*shapes_mem_count + trainable_count + non_trainable_count)
      gbytes = np.round(total_memory / (1024.0 ** 3), 3) + internal_model_mem_count
      return gbytes

   def gen_label_test(self, imglist):
      #img_extension = 'png'
      for file in imglist:
         if file.endswith('.bin'):
            imagefilename = os.path.splitext(file)[0] + '.' + self.img_extension
            img = load_img(os.path.sep.join([self.imgfolder, imagefilename]))
            outputfilename = os.path.splitext(file)[0] + '_bin' + '.' + self.img_extension
            self.bin2format(self.label_type, os.path.sep.join([self.binfolder, file]), img.size[1], img.size[0], 
                            os.path.sep.join([self.labelfolder, imagefilename]), self.labels)
            self.outputfilenames.append(outputfilename)

   """ def gen_label_test2(self, imgname):
      img_extension = 'png'
      if imgname.endswith('.bin'):
         imagefilename = os.path.splitext(imgname)[0] + '.' + img_extension
         img = load_img(os.path.sep.join([self.imgfolder, imagefilename]))
         outputfilename = os.path.splitext(imgname)[0] + '_bin' + '.' + img_extension
         self.bin2format('segmented_image', os.path.sep.join([self.binfolder, file]), img.size[1], img.size[0], 
                         os.path.sep.join([self.labelfolder, outputfilename]), self.labels)
         self.outputfilenames.append(outputfilename) """

   def gen_label_in_series(self, imgfolder, binfolder, labelfolder, labels):

      self.outputfilenames = []
      self.imgfolder = imgfolder
      self.binfolder = binfolder
      self.labelfolder = labelfolder
      self.labels = labels

      if os.path.exists(imgfolder):
         self.lower_case_ext(imgfolder)

      files = sorted(os.listdir(binfolder))

      self.gen_label_test(files)

      return self.outputfilenames

   def gen_label_multiprocess(self, imgfolder, binfolder, labelfolder, labels, num_cpus, auto_num_cpus, img_extension, label_type):
      
      self.outputfilenames = []
      self.imgfolder = imgfolder
      self.binfolder = binfolder
      self.labelfolder = labelfolder
      self.labels = labels
      self.img_extension = img_extension
      self.label_type = label_type

      if not os.path.exists(self.labelfolder):
         os.makedirs(self.labelfolder)

      print('lower case extension files')

      if os.path.exists(self.imgfolder):
         self.lower_case_ext(self.imgfolder)

      print('get list of files')

      files = sorted(os.listdir(self.binfolder))

      print(files)

      print('create process pool')

      workers = num_cpus if not auto_num_cpus else cpu_count()

      workers = workers if len(files) >= workers else len(files)

      print('number of workers: {}'.format(workers))

      chunk_list = self.chunk_data(files, workers)

      with concurrent.futures.ProcessPoolExecutor(max_workers=workers) as executor:
         for i in range(0, workers):
            executor.submit(self.gen_label_test, chunk_list[i])

      return self.outputfilenames

   def chunk_data(self, files, workers):

      total_files = len(files)
      files_per_worker = math.ceil(total_files/workers)

      chunk_list = []

      for i in range (0, workers):
         start_idx = i*files_per_worker
         if i == workers-1:
            end_idx = start_idx + (total_files - start_idx)
         else:
            end_idx = start_idx + files_per_worker

         chunk_list.append(files[start_idx:end_idx])
         
      return chunk_list
