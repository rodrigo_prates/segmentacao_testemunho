import matplotlib.pyplot as plt
from collections import Counter
import numpy as np
import os

from matplotlib.ticker import MultipleLocator

#from ImageUtils import ImageUtils


class PostProcessImages:
   
   def __init__(self, debug, logfolder, logfile):
      self.debug = True if debug == 1 else False
      self.logfolder = logfolder
      if not os.path.exists(logfolder):
         os.makedirs(logfolder)
      self.logfile = logfile


   def labelshist(self, outfolder, labels):
      return dict(Counter(labels))

   def discardlabels(self, threshold):
      pass

   def plottrainning(self, H, N, plotpath):
      plt.style.use('ggplot')
      plt.figure()
      #dictkeys = H.history.keys()
      dictkeys = H.keys()
      #plt.plot(np.arange(0, N), H.history["loss"], label="train_loss")
      plt.plot(np.arange(0, N), H["loss"], label="train_loss")
      #plt.plot(np.arange(0, N), H.history["val_loss"], label="val_loss")
      plt.plot(np.arange(0, N), H["val_loss"], label="val_loss")
      if 'acc' in dictkeys:
         #plt.plot(np.arange(0, N), H.history["acc"], label="train_acc")
         plt.plot(np.arange(0, N), H["acc"], label="train_acc")
      #   plt.plot(np.arange(0, N), H.history["val_acc"], label="val_acc")
         plt.plot(np.arange(0, N), H["val_acc"], label="val_acc")
      else:
         #plt.plot(np.arange(0, N), H.history["accuracy"], label="train_acc")
         plt.plot(np.arange(0, N), H["accuracy"], label="train_acc")
      #   plt.plot(np.arange(0, N), H.history["val_accuracy"], label="val_acc")
         plt.plot(np.arange(0, N), H["val_accuracy"], label="val_acc")
      plt.title('Trainning Loss and Accuracy')
      plt.xlabel('Epoch #')
      plt.ylabel('Loss/Accuracy')
      plt.legend(loc='lower left')
      plt.savefig(plotpath)

   def stories2dict(self, stories):
      history = {}
      losses = []
      accuracies = []
      val_losses = []
      val_accuracies = []
      for st in stories:
         losses.append(st['loss'])
         val_losses.append(st['val_loss'])
         accuracies.append(st['accuracy'])
         val_accuracies.append(st['val_accuracy'])

      history['loss'] = losses
      history['accuracy'] = accuracies
      history['val_loss'] = val_losses
      history['val_accuracy'] = val_accuracies
      return history

   def plotacc(self, acc_list, plotpath, x_axis_labels=None):
      #img_utils = ImageUtils(True, self.logfolder, self.logfile)
      #img_utils.write2log(self.logfile, 'Plot Accuracies - {}'.format(datetime.today().strftime('%H:%M:%S')))

      plt.style.use('ggplot')
      plt.figure()

      plt.bar(np.arange(0, len(acc_list)), acc_list, align='center')
      plt.title('Models Accuracy')
      if x_axis_labels:
         plt.xticks(range(len(x_axis_labels)), x_axis_labels, size='small')
      plt.xlabel('Models #')
      plt.ylabel('Accuracy')
      #plt.legend(loc='lower left')
      plt.savefig(plotpath)

   def plotConfMatrix(self, confMatrix, labels, plotpath):
      fig = plt.figure()
      ax = fig.add_subplot(111)
      cax = ax.matshow(confMatrix)
      plt.title('Confusion matrix')
      fig.colorbar(cax)
      ax.set_xticklabels([''] + labels)
      ax.set_yticklabels([''] + labels)
      ax.xaxis.set_major_locator(MultipleLocator(1))
      ax.yaxis.set_major_locator(MultipleLocator(1))
      plt.xlabel('Predicted')
      plt.ylabel('True')
      plt.savefig(plotpath)      
