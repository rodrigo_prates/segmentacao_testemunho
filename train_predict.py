import os
import config

os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
if config.USE_GPU:
   if config.GPU_ID:
      os.environ["CUDA_VISIBLE_DEVICES"] = config.GPU_ID
else:
   os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

from SegImages import SegImages
from PostProcessImages import PostProcessImages
from PreProcessImages import PreProcessImages
from ImageUtils import ImageUtils
from datetime import datetime

from keras import backend as K


INPUT_FOLDER = config.INPUT_IMAGES_FOLDER
LABEL_FOLDER = config.LABEL_IMAGES_FOLDER
BIN_FOLDER = config.BIN_IMAGES_FOLDER
ANN_FOLDER = config.ANN_IMAGES_FOLDER
OUTPUT_FOLDER = config.OUTPUT_FOLDER
WINDOWS_SIZE = [config.WINDOW_WIDTH, config.WINDOW_HEIGHT]
WINDOW_MODE = config.WINDOW_MODE
WINDOW_STEP = config.WINDOW_STEP
WINDOW_OFFSET = config.WINDOW_OFFSET
AUGMENT = config.AUGMENT

IMAGE_EXTENSION = config.IMAGE_EXTENSION
DEBUG = config.DEBUG
TRAINSIZE = config.TRAINSIZE

OUTPUT_CORE_FOLDER = config.OUTPUT_CORE_FOLDER
OUTPUT_LABEL_FOLDER = config.OUTPUT_LABEL_FOLDER
OUTPUT_PREDICTION_FOLDER = config.OUTPUT_PREDICTION_FOLDER

BATCH_SIZE=config.BATCH_SIZE
IMAGENET_MEAN = config.IMAGENET_MEAN
NUM_LABELS=config.NUM_LABELS
AUTO_BATCH_SIZE = config.AUTO_BATCH_SIZE
NETMODEL = config.NETMODEL
SEGMODEL = config.SEGMODEL
RETRAIN = config.RETRAIN
NUMEPOCHS = config.NUMEPOCHS

LABELS = config.LABELS

LOGS_FOLDER = config.LOGS_FOLDER
logfile = os.path.sep.join([LOGS_FOLDER, 'logs_' + datetime.today().strftime('%Y_%m_%d') + '.txt'])

VERIFY_DATASET = config.VERIFY_DATASET

NUM_GPUS = config.NUM_GPUS
USE_GPU = config.USE_GPU

segimages = SegImages(DEBUG, LOGS_FOLDER, logfile, USE_GPU, NUM_GPUS)
img_utils = ImageUtils(True, LOGS_FOLDER, logfile)

input_trainpath = os.path.sep.join([OUTPUT_FOLDER, 'trainning_input'])
output_trainpath = os.path.sep.join([OUTPUT_FOLDER, 'trainning_output'])

input_valpath = os.path.sep.join([OUTPUT_FOLDER, 'validation_input'])
output_valpath = os.path.sep.join([OUTPUT_FOLDER, 'validation_output'])

input_testpath = os.path.sep.join([OUTPUT_FOLDER, 'testing_input'])
output_testpath = os.path.sep.join([OUTPUT_FOLDER, 'testing_output'])

# SEGMODELS = ["vgg_unet", 
#              "resnet50_unet", 
#              "mobilenet_unet", 
#              "vgg_segnet", 
#              "segnet", 
#              "resnet50_segnet", 
#              "mobilenet_segnet", 
#              "pspnet", 
#              "vgg_pspnet", 
#              "resnet50_pspnet", 
#              "fcn_8", 
#              "fcn_32", 
#              "fcn_8_vgg", 
#              "fcn_32_vgg", 
#              "fcn_8_resnet50", 
#              "fcn_32_resnet50", 
#              "fcn_8_mobilenet", 
#              "fcn_32_mobilenet"]

SEGMODELS = ["mobilenet_unet"]

modelname = 'mobilenet_unet'
modelpath = 'output/' + modelname + '/' + modelname
modelfilepath = None
segimages.segtrainmultiple(input_trainpath, input_valpath, output_trainpath, output_valpath, SEGMODELS, AUTO_BATCH_SIZE, BATCH_SIZE, NUM_LABELS, NUMEPOCHS, 
	OUTPUT_FOLDER, VERIFY_DATASET, modelpath, modelfilepath, config.WINDOW_HEIGHT, config.WINDOW_WIDTH)

#image_folder = os.path.sep.join([OUTPUT_FOLDER, 'testing_input']) #'/home/petrec/Downloads/segmentacao_testemunho/output/testing_input'
#annotation_folder = os.path.sep.join([OUTPUT_FOLDER, 'testing_output'])
#modelfilepath = os.path.sep.join([OUTPUT_FOLDER, modelname, 'mobilenet_segnet.18'])
NUM_IMAGES_TO_TEST = 100

#segimages.segprediction(modelpath, input_testpath, output_testpath, OUTPUT_PREDICTION_FOLDER, modelname, NUM_LABELS, None, NUM_IMAGES_TO_TEST)

imgfilepath = os.path.sep.join([INPUT_FOLDER, "3-RJS-749D-RJ_5.374-60-5.376-55_FotoHDI_T2_CX19-21.png"]) #'images/cores/2ANP-2A_5.548-00-5.550-70_FotoHDI_T01_CX1-3.png'
if 'mobilenet' in modelname:
   K.set_image_data_format('channels_last')
img_utils.paintpredicted(imgfilepath, LABELS, modelpath, WINDOWS_SIZE, WINDOW_STEP, WINDOW_MODE, WINDOW_OFFSET, OUTPUT_PREDICTION_FOLDER, modelname, None)

#imgfilepath = os.path.sep.join([LABEL_FOLDER, "2ANP-2A_5.548-00-5.550-70_FotoHDI_T01_CX1-3_bin.png"])
#predictedfilepath = os.path.sep.join([OUTPUT_PREDICTION_FOLDER, modelname, "predseg_2ANP-2A_5.548-00-5.550-70_FotoHDI_T01_CX1-3.png"])
#img_utils.compare_images(imgfilepath, predictedfilepath, OUTPUT_PREDICTION_FOLDER)
