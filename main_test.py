from SegImages import SegImages, PostProcessImages, UtilsImages
import config


IMAGENET_MEAN = config.IMAGENET_MEAN
NETMODEL = config.NETMODEL
DEBUG = config.DEBUG
LABELS = config.LABELS
WINDOWS_SIZE = [ config.WINDOW_WIDTH, config.WINDOW_HEIGHT]
WINDOW_MODE = config.WINDOW_MODE
WINDOW_STEP = config.WINDOW_STEP
WINDOW_OFFSET = config.WINDOW_OFFSET
IMAGE_EXTENSION = config.IMAGE_EXTENSION
PIXEL_LENGTH = config.PIXEL_LENGTH

segimages = SegImages(DEBUG)

targetsize=segimages.TRAGETSIZE[NETMODEL]
inputsize=(targetsize[0],targetsize[1],3)

modelpath = '/home/petrec/Downloads/cnn_for_core_lithof_classif/transfer-learning-hdi/output/' + NETMODEL + '/core_net.model'

labelfolder1 = '/home/petrec/Downloads/cnn_for_core_lithof_classif/transfer-learning-hdi/images/Manha_1.csv'
imgfolder1 = '/home/petrec/Downloads/cnn_for_core_lithof_classif/transfer-learning-hdi/images/Manha_1.png'

labelfolder2 = '/home/petrec/Downloads/cnn_for_core_lithof_classif/transfer-learning-hdi/images/Manha_2.csv'
imgfolder2 = '/home/petrec/Downloads/cnn_for_core_lithof_classif/transfer-learning-hdi/images/Manha_2.png'

labelfolder3 = '/home/petrec/Downloads/cnn_for_core_lithof_classif/transfer-learning-hdi/images/Manha_3.csv'
imgfolder3 = '/home/petrec/Downloads/cnn_for_core_lithof_classif/transfer-learning-hdi/images/Manha_3.png'

labelfolder4 = '/home/petrec/Downloads/cnn_for_core_lithof_classif/transfer-learning-hdi/images/Manha_4.csv'
imgfolder4 = '/home/petrec/Downloads/cnn_for_core_lithof_classif/transfer-learning-hdi/images/Manha_4.png'

OUTPUT_FOLDER = config.OUTPUT_FOLDER

COLORSDICT = UtilsImages.getlabelcolors(LABELS)

THR = config.CLASSIFICATION_THR
CSV_SEP = config.CSV_SEP
IMAGENET_MEAN = config.IMAGENET_MEAN

predictlabels, labels = UtilsImages.corepaint(imgfolder1, labelfolder1, CSV_SEP, OUTPUT_FOLDER, WINDOWS_SIZE, 
   WINDOW_MODE, WINDOW_STEP, WINDOW_OFFSET, IMAGENET_MEAN, modelpath, targetsize, LABELS, COLORSDICT, THR, DEBUG, IMAGE_EXTENSION, NETMODEL, PIXEL_LENGTH)

print(segimages.regions_report(predictlabels, labels))

predictlabels, labels = UtilsImages.corepaint(imgfolder2, labelfolder2, CSV_SEP, OUTPUT_FOLDER, WINDOWS_SIZE, 
   WINDOW_MODE, WINDOW_STEP, WINDOW_OFFSET, IMAGENET_MEAN, modelpath, targetsize, LABELS, COLORSDICT, THR, DEBUG, IMAGE_EXTENSION, NETMODEL, PIXEL_LENGTH)

print(segimages.regions_report(predictlabels, labels))

predictlabels, labels = UtilsImages.corepaint(imgfolder3, labelfolder3, CSV_SEP, OUTPUT_FOLDER, WINDOWS_SIZE, 
   WINDOW_MODE, WINDOW_STEP, WINDOW_OFFSET, IMAGENET_MEAN, modelpath, targetsize, LABELS, COLORSDICT, THR, DEBUG, IMAGE_EXTENSION, NETMODEL, PIXEL_LENGTH)

print(segimages.regions_report(predictlabels, labels))

predictlabels, labels = UtilsImages.corepaint(imgfolder4, labelfolder4, CSV_SEP, OUTPUT_FOLDER, WINDOWS_SIZE, 
   WINDOW_MODE, WINDOW_STEP, WINDOW_OFFSET, IMAGENET_MEAN, modelpath, targetsize, LABELS, COLORSDICT, THR, DEBUG, IMAGE_EXTENSION, NETMODEL, PIXEL_LENGTH)

print(segimages.regions_report(predictlabels, labels))
