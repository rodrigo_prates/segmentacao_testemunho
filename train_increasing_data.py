import os
import config

os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
if config.USE_GPU:
   if config.GPU_ID:
      os.environ["CUDA_VISIBLE_DEVICES"] = config.GPU_ID
else:
   os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

from SegImages import SegImages
from PostProcessImages import PostProcessImages
from PreProcessImages import PreProcessImages
from ImageUtils import ImageUtils
from datetime import datetime

import random
import shutil
import numpy as np


INPUT_FOLDER = config.INPUT_IMAGES_FOLDER
LABEL_FOLDER = config.LABEL_IMAGES_FOLDER
BIN_FOLDER = config.BIN_IMAGES_FOLDER
ANN_FOLDER = config.ANN_IMAGES_FOLDER
OUTPUT_FOLDER = config.OUTPUT_FOLDER
WINDOWS_SIZE = [config.WINDOW_WIDTH, config.WINDOW_HEIGHT]
WINDOW_MODE = config.WINDOW_MODE
WINDOW_STEP = config.WINDOW_STEP
WINDOW_OFFSET = config.WINDOW_OFFSET
AUGMENT = config.AUGMENT

IMAGE_EXTENSION = config.IMAGE_EXTENSION
DEBUG = config.DEBUG
TRAINSIZE = config.TRAINSIZE

OUTPUT_CORE_FOLDER = config.OUTPUT_CORE_FOLDER
OUTPUT_LABEL_FOLDER = config.OUTPUT_LABEL_FOLDER

BATCH_SIZE=config.BATCH_SIZE
IMAGENET_MEAN = config.IMAGENET_MEAN
NUM_LABELS=config.NUM_LABELS
AUTO_BATCH_SIZE = config.AUTO_BATCH_SIZE
NETMODEL = config.NETMODEL
SEGMODEL = config.SEGMODEL
RETRAIN = config.RETRAIN
NUMEPOCHS = config.NUMEPOCHS

LABELS = config.LABELS

LOGS_FOLDER = config.LOGS_FOLDER
logfile = os.path.sep.join([LOGS_FOLDER, 'logs_' + datetime.today().strftime('%Y_%m_%d') + '.txt'])

VERIFY_DATASET = config.VERIFY_DATASET

NUM_GPUS = config.NUM_GPUS
USE_GPU = config.USE_GPU

segimages = SegImages(DEBUG, LOGS_FOLDER, logfile, USE_GPU, NUM_GPUS)
img_utils = ImageUtils(True, LOGS_FOLDER, logfile)

increase_dataset = [1,3,6]
SEGMODELS = ["mobilenet_unet"]

for id in increase_dataset:

   print('train model with {} core image ....'.format(str(id)))

   NEW_INPUT_FOLDER = INPUT_FOLDER+str(id)
   NEW_ANN_FOLDER = ANN_FOLDER+str(id)
   NEW_OUTPUT_FOLDER = OUTPUT_FOLDER+str(id)

   NEW_OUTPUT_CORE_FOLDER = os.path.sep.join([NEW_OUTPUT_FOLDER, 'cores']) 
   NEW_OUTPUT_LABEL_FOLDER = os.path.sep.join([NEW_OUTPUT_FOLDER, 'labels'])
   all_files = os.listdir(INPUT_FOLDER)
   #print(all_files)
   indexes = np.random.randint(len(all_files), size=id)
   #print(indexes)

   files = [all_files[i] for i in indexes]
   #print(files)

   if not os.path.exists(NEW_INPUT_FOLDER):
      os.makedirs(NEW_INPUT_FOLDER)

   if not os.path.exists(NEW_ANN_FOLDER):
      os.makedirs(NEW_ANN_FOLDER)

   if len(os.listdir(NEW_INPUT_FOLDER)) == 0:
      for f in files:
         newfilepath = os.path.sep.join([NEW_INPUT_FOLDER, f])
         origfilepath = os.path.sep.join([INPUT_FOLDER, f])
         shutil.copy2(origfilepath, newfilepath)

   if len(os.listdir(NEW_ANN_FOLDER)) == 0:
      for f in files:
         newfilepath = os.path.sep.join([NEW_ANN_FOLDER, f])
         origfilepath = os.path.sep.join([ANN_FOLDER, f])
         shutil.copy2(origfilepath, newfilepath)

   print('Generate Data for trainning, validate and test ....')

   segimages.gen_data(NEW_INPUT_FOLDER, NEW_ANN_FOLDER, NEW_OUTPUT_CORE_FOLDER, NEW_OUTPUT_LABEL_FOLDER, WINDOWS_SIZE, WINDOW_MODE, WINDOW_STEP, WINDOW_OFFSET, AUGMENT, IMAGE_EXTENSION)
   core_train, core_test, core_val, label_train, label_test, label_val = img_utils.datasplittingfolder(NEW_OUTPUT_CORE_FOLDER, NEW_OUTPUT_LABEL_FOLDER, TRAINSIZE)

   input_trainpath, output_trainpath = img_utils.organizedataset('trainning', NEW_OUTPUT_CORE_FOLDER, NEW_OUTPUT_LABEL_FOLDER, NEW_OUTPUT_FOLDER, core_train, label_train)
   input_valpath, output_valpath = img_utils.organizedataset('validation', NEW_OUTPUT_CORE_FOLDER, NEW_OUTPUT_LABEL_FOLDER, NEW_OUTPUT_FOLDER, core_val, label_val)
   input_testpath, output_testpath = img_utils.organizedataset('testing', NEW_OUTPUT_CORE_FOLDER, NEW_OUTPUT_LABEL_FOLDER, NEW_OUTPUT_FOLDER, core_test, label_test)

   print('Train model '.format(str(SEGMODELS)))

   segimages.segtrainmultiple(input_trainpath, input_valpath, output_trainpath, output_valpath, SEGMODELS, AUTO_BATCH_SIZE, BATCH_SIZE, NUM_LABELS, NUMEPOCHS, NEW_OUTPUT_FOLDER, VERIFY_DATASET)

   print('Remove output image folders ....')

   shutil.rmtree(NEW_OUTPUT_CORE_FOLDER)
   shutil.rmtree(NEW_OUTPUT_LABEL_FOLDER)
   #shutil.rmtree(NEW_OUTPUT_FOLDER)
