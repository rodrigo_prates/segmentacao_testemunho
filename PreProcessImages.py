import os
import numpy as np
import cv2
import imutils
import PIL


class PreProcessImages:

   def __init__(self):
      self.debug = True

   def resizing(self, image, newwidth=1080, newheight=1920):
      outimage = image.resize(( newwidth, newheight ))
      return outimage

   def flipimage(self, image):
      outimage = image.transpose(PIL.Image.FLIP_LEFT_RIGHT)
      return outimage

   def check_image_orientation(self, imgfolder, img_extension='png'):
      for file in sorted(os.listdir(imgfolder)):
         if file.lower().endswith('.png'):
            try:
               image = cv2.imread(os.path.sep.join([imgfolder, file]))
               height, width, channels = image.shape
               if width > height:
                  image_rotated = imutils.rotate_bound(image, 90)
                  cv2.imwrite(os.path.sep.join([imgfolder, file]), image_rotated)
            except IOError as e:
               print(e)

   def concat_images(self, imgfolder, imglist, output_folder, output_file_name):
      num_images = len(imglist)
      if num_images > 1:
         try:
            image = cv2.imread(os.path.sep.join([imgfolder, imglist[0]]))
            height, width, channels = image.shape
            for idx in range(1, len(imglist)):
               temp_image = cv2.imread(os.path.sep.join([imgfolder, imglist[idx]]))
               temp_image = imutils.resize(temp_image, width=width)
               image = np.concatenate((image, temp_image), axis=0)

            cv2.imwrite(os.path.sep.join([output_folder, output_file_name]), image)
         except:
            print('Error reading image!')

      else:
         print('cannot join just one image!')
