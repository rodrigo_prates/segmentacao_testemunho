from SegImages import PreProcessImages
import argparse


ap = argparse.ArgumentParser()
ap.add_argument("-i", "--imgpath", required=True, help="input folder")
ap.add_argument("-f", "--files", nargs='*', required=True, help="file list")
ap.add_argument("-o", "--outfile", required=True, help="output file name")
args = vars(ap.parse_args())

INPUT_FOLDER = args['imgpath']
IMAGE_EXTENSION = 'png'

PreProcessImages.check_image_orientation(INPUT_FOLDER, IMAGE_EXTENSION)

FILES = args['files']

OUTPUT_FOLDER = args['imgpath']
OUTPUT_FILE = args['outfile']

PreProcessImages.concat_images(INPUT_FOLDER, FILES, OUTPUT_FOLDER, OUTPUT_FILE)