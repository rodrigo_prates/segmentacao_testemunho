from SegImages import SegImages, PostProcessImages, UtilsImages, PreProcessImages
import config


INPUT_FOLDER = config.INPUT_IMAGES_FOLDER
OUTPUT_FOLDER = config.OUTPUT_FOLDER
WINDOWS_SIZE = [ config.WINDOW_WIDTH, config.WINDOW_HEIGHT]
WINDOW_MODE = config.WINDOW_MODE
WINDOW_STEP = config.WINDOW_STEP
WINDOW_OFFSET = config.WINDOW_OFFSET
AUGMENT = config.AUGMENT
CSV_SEP = config.CSV_SEP
IMAGE_EXTENSION = config.IMAGE_EXTENSION
DEBUG = config.DEBUG
PIXEL_LENGTH = config.PIXEL_LENGTH

PreProcessImages.check_image_orientation(INPUT_FOLDER, IMAGE_EXTENSION)

segimages = SegImages(DEBUG)
slidenames_list, labels_list = segimages.gen_data(INPUT_FOLDER, 
                                                 OUTPUT_FOLDER, 
                                                 WINDOWS_SIZE,
                                                 WINDOW_MODE, 
                                                 WINDOW_STEP, 
                                                 WINDOW_OFFSET, 
                                                 AUGMENT, 
                                                 CSV_SEP, 
                                                 IMAGE_EXTENSION, 
                                                 PIXEL_LENGTH)

UtilsImages.write_lists_to_csv(OUTPUT_FOLDER, slidenames_list, labels_list, CSV_SEP, ['image', 'label'], 'dataset.csv')

TRAINSIZE = config.TRAINSIZE
slide_train, slide_test, slide_val, label_train, label_test, label_val = UtilsImages.datasplitting(slidenames_list, labels_list, TRAINSIZE)

slide_train, slide_val, label_train, label_val = segimages.filterneighbors(slidenames_list, slide_train, slide_test, slide_val, label_train, label_val)

trainpath = UtilsImages.organizedataset('training', OUTPUT_FOLDER, OUTPUT_FOLDER, slide_train)
valpath = UtilsImages.organizedataset('validation', OUTPUT_FOLDER, OUTPUT_FOLDER, slide_val)
testpath = UtilsImages.organizedataset('testing', OUTPUT_FOLDER, OUTPUT_FOLDER, slide_test)

UtilsImages.removeimages(OUTPUT_FOLDER)

labelsdata = PostProcessImages.labelshist(OUTPUT_FOLDER, label_train)
UtilsImages.write_dict_to_csv(labelsdata, OUTPUT_FOLDER)

BATCH_SIZE=config.BATCH_SIZE
IMAGENET_MEAN = config.IMAGENET_MEAN
NUM_LABELS=len(config.LABELS)
AUTO_BATCH_SIZE = config.AUTO_BATCH_SIZE
NETMODEL = config.NETMODEL
RETRAIN = config.RETRAIN
NUMEPOCHS = config.NUMEPOCHS

train_features_path, val_features_path, test_features_path = segimages.feature_extraction(trainpath, 
	                                                                                      valpath, 
	                                                                                      testpath, 
	                                                                                      NETMODEL, 
	                                                                                      AUTO_BATCH_SIZE, 
	                                                                                      BATCH_SIZE, 
	                                                                                      IMAGENET_MEAN, 
	                                                                                      len(slide_train), 
	                                                                                      len(slide_val), 
	                                                                                      len(slide_test), 
	                                                                                      OUTPUT_FOLDER, 
	                                                                                      label_train, 
	                                                                                      label_val, 
	                                                                                      label_test, 
	                                                                                      config.LABELS, 
	                                                                                      CSV_SEP)

segimages.train_from_features(train_features_path, 
	                          val_features_path, 
	                          test_features_path, 
	                          config.LABELS, 
	                          OUTPUT_FOLDER, 
	                          config.CLASSIFIER, config.MAX_ITER)

modelpath = segimages.trainning(trainpath, 
                                valpath,
                                testpath,
                                NETMODEL, 
                                AUTO_BATCH_SIZE, 
                                BATCH_SIZE, 
                                IMAGENET_MEAN, 
                                NUM_LABELS, 
                                len(slide_train), 
                                len(slide_val), 
                                len(slide_test), 
                                NUMEPOCHS, 
                                OUTPUT_FOLDER, 
                                RETRAIN)
