from ImageUtils import ImageUtils
import os
import config
from datetime import datetime


DEBUG = config.DEBUG
LOGS_FOLDER = config.LOGS_FOLDER
logfile = os.path.sep.join([LOGS_FOLDER, 'logs_' + datetime.today().strftime('%Y_%m_%d') + '.txt'])

img_utils = ImageUtils(DEBUG, LOGS_FOLDER, logfile)

labelfile = '/home/petrec/Downloads/segmentacao_testemunho/images/labels/2ANP-2A_5.548-00-5.550-70_FotoHDI_T01_CX1-3_bin.png'
predictedfile = '/home/petrec/Downloads/segment_images_train_gpu/21-01-2020/predictions/vgg_unet/predseg_2ANP-2A_5.548-00-5.550-70_FotoHDI_T01_CX1-3.png'
resultpath = '/home/petrec/Downloads/segmentacao_testemunho/output/predictions'

img_utils.compare_images(labelfile, predictedfile, resultpath)