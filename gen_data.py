import os
import config

os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
if config.USE_GPU:
   if config.GPU_ID:
      os.environ["CUDA_VISIBLE_DEVICES"] = config.GPU_ID
else:
   os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

from SegImages import SegImages
from PostProcessImages import PostProcessImages
from PreProcessImages import PreProcessImages
from ImageUtils import ImageUtils
from datetime import datetime


INPUT_FOLDER = config.INPUT_IMAGES_FOLDER
LABEL_FOLDER = config.LABEL_IMAGES_FOLDER
BIN_FOLDER = config.BIN_IMAGES_FOLDER
ANN_FOLDER = config.ANN_IMAGES_FOLDER
OUTPUT_FOLDER = config.OUTPUT_FOLDER
WINDOWS_SIZE = [config.WINDOW_WIDTH, config.WINDOW_HEIGHT]
WINDOW_MODE = config.WINDOW_MODE
WINDOW_STEP = config.WINDOW_STEP
WINDOW_OFFSET = config.WINDOW_OFFSET
AUGMENT = config.AUGMENT

IMAGE_EXTENSION = config.IMAGE_EXTENSION
DEBUG = config.DEBUG
TRAINSIZE = config.TRAINSIZE

OUTPUT_CORE_FOLDER = config.OUTPUT_CORE_FOLDER
OUTPUT_LABEL_FOLDER = config.OUTPUT_LABEL_FOLDER

BATCH_SIZE=config.BATCH_SIZE
IMAGENET_MEAN = config.IMAGENET_MEAN
NUM_LABELS=config.NUM_LABELS
AUTO_BATCH_SIZE = config.AUTO_BATCH_SIZE
NETMODEL = config.NETMODEL
SEGMODEL = config.SEGMODEL
RETRAIN = config.RETRAIN
NUMEPOCHS = config.NUMEPOCHS

LABELS = config.LABELS

LOGS_FOLDER = config.LOGS_FOLDER
logfile = os.path.sep.join([LOGS_FOLDER, 'logs_' + datetime.today().strftime('%Y_%m_%d') + '.txt'])

VERIFY_DATASET = config.VERIFY_DATASET

NUM_GPUS = config.NUM_GPUS
USE_GPU = config.USE_GPU

NUM_CPUS = config.NUM_CPUS
AUTO_NUM_CPUS = config.AUTO_NUM_CPUS

segimages = SegImages(DEBUG, LOGS_FOLDER, logfile, USE_GPU, NUM_GPUS)
img_utils = ImageUtils(True, LOGS_FOLDER, logfile)

#segimages.gen_labels_images(INPUT_FOLDER, LABEL_FOLDER, BIN_FOLDER, ANN_FOLDER, LABELS, IMAGE_EXTENSION)
#img_utils.gen_label_multiprocess(INPUT_FOLDER, BIN_FOLDER, LABEL_FOLDER, LABELS, NUM_CPUS, AUTO_NUM_CPUS, IMAGE_EXTENSION)
LABEL_TYPE = 'annotated_image'
img_utils.gen_label_multiprocess(INPUT_FOLDER, BIN_FOLDER, ANN_FOLDER, LABELS, NUM_CPUS, AUTO_NUM_CPUS, IMAGE_EXTENSION, LABEL_TYPE)

#segimages.gen_data(INPUT_FOLDER, ANN_FOLDER, OUTPUT_CORE_FOLDER, OUTPUT_LABEL_FOLDER, WINDOWS_SIZE, WINDOW_MODE, WINDOW_STEP, WINDOW_OFFSET, AUGMENT, IMAGE_EXTENSION)
#segimages.gen_data(INPUT_FOLDER, ANN_FOLDER, OUTPUT_CORE_FOLDER, OUTPUT_LABEL_FOLDER, WINDOWS_SIZE, WINDOW_MODE, 
#                   WINDOW_STEP, WINDOW_OFFSET, IMAGE_EXTENSION, NUM_CPUS, AUTO_NUM_CPUS)

#core_train, core_test, core_val, label_train, label_test, label_val = img_utils.datasplittingfolder(OUTPUT_CORE_FOLDER, OUTPUT_LABEL_FOLDER, TRAINSIZE)

#input_trainpath, output_trainpath = img_utils.organizedataset('trainning', OUTPUT_CORE_FOLDER, OUTPUT_LABEL_FOLDER, OUTPUT_FOLDER, core_train, label_train)
#input_valpath, output_valpath = img_utils.organizedataset('validation', OUTPUT_CORE_FOLDER, OUTPUT_LABEL_FOLDER, OUTPUT_FOLDER, core_val, label_val)
#input_testpath, output_testpath = img_utils.organizedataset('testing', OUTPUT_CORE_FOLDER, OUTPUT_LABEL_FOLDER, OUTPUT_FOLDER, core_test, label_test)
