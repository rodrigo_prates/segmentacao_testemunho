import numpy as np
import os


LABELS = {(51,102,0): 0,
          (0, 102, 102): 1, 
          (255, 51, 153): 2, 
          (255, 124, 128): 3, 
          (255, 153, 204): 4, 
          (112, 48, 160): 5, 
          (153, 51, 255): 6,
          (204, 0, 255): 7,
          (204, 102, 255): 8, 
          (102, 0, 255): 9, 
          (204, 153, 255): 10, 
          (0, 0, 255): 11, 
          (51, 102, 255): 12, 
          (102, 255, 255): 13, 
          (255, 153,0): 14,
          (255, 0, 0): 15, 
          (255, 51, 0): 16, 
          (0, 0, 0): 17}

LABEL_NAMES = ["ARG", 
                "LAM", 
                "EMA", 
                "ELA", 
                "ECA", 
                "ARB", 
                "ARS", 
                "AVG", 
                "ARC", 
                "DOM", 
                "CRO", 
                "GST", 
                "RUD", 
                "COQ",
                "SLX",
                "IGN",
                "OUT",
                "BKG"]

NETMODEL = 'vgg16'
CLASSIFIER = 'logist_regression'
SEGMODEL = 'vgg_unet'

WINDOW_WIDTH = 224
WINDOW_HEIGHT = 224
WINDOW_MODE = 'horizontal'
WINDOW_STEP = 60
WINDOW_OFFSET = 0
AUGMENT = 1
CSV_SEP = ','
IMAGE_EXTENSION = 'png'

DIRPATH = os.getcwd() #'/home/petrec/Downloads/segment_images'

INPUT_IMAGES_FOLDER = os.path.sep.join([DIRPATH, 'images', 'cores5']) #'/home/petrec/Downloads/segment_images/images/cores'
LABEL_IMAGES_FOLDER = os.path.sep.join([DIRPATH, 'images', 'labels']) #'/home/petrec/Downloads/segment_images/images/labels'
BIN_IMAGES_FOLDER = os.path.sep.join([DIRPATH, 'images', 'bin']) #'/home/petrec/Downloads/segment_images/images/bin'
ANN_IMAGES_FOLDER = os.path.sep.join([DIRPATH, 'images', 'annotated5']) #'/home/petrec/Downloads/segment_images/images/annotated'
OUTPUT_FOLDER = os.path.sep.join([DIRPATH, 'output']) #'/home/petrec/Downloads/segment_images/output'

OUTPUT_CORE_FOLDER = os.path.sep.join([OUTPUT_FOLDER, 'cores5']) #'/home/petrec/Downloads/segment_images/output/cores'
OUTPUT_LABEL_FOLDER = os.path.sep.join([OUTPUT_FOLDER, 'labels5']) #'/home/petrec/Downloads/segment_images/output/labels'
#OUTPUT_ANN_FOLDER = '/home/petrec/Downloads/segment_images/output/annotated'
OUTPUT_PREDICTION_FOLDER = os.path.sep.join([OUTPUT_FOLDER, 'predictions']) #'/home/petrec/Downloads/segment_images/output/predictions'

DEBUG = 1

TRAINSIZE = 75

BATCH_SIZE = 32
IMAGENET_MEAN = np.array([123.68, 116.779, 103.939], dtype="float32")
NUM_LABELS=len(LABELS)

AUTO_BATCH_SIZE = False
RETRAIN = True
NUMEPOCHS = 60

CLASSIFICATION_THR = 0.3
PIXEL_LENGTH = 3.6e-05
MAX_ITER = 1000

LOGS_FOLDER = os.path.sep.join([OUTPUT_FOLDER, 'logs']) #'/home/petrec/Downloads/segment_images/output/logs'

VERIFY_DATASET = False

USE_GPU = True

GPU_ID = "0" # use "0" or "1" or None

NUM_GPUS = 2

NUM_CPUS = 2
AUTO_NUM_CPUS = True
