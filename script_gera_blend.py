from PIL import Image
import PIL
PIL.Image.MAX_IMAGE_PIXELS = 933120000


def changeImageSize(maxWidth, maxHeight, image):
    widthRatio  = maxWidth/image.size[0]
    heightRatio = maxHeight/image.size[1]
    newWidth    = int(widthRatio*image.size[0])
    newHeight   = int(heightRatio*image.size[1])
    newImage    = image.resize((newWidth, newHeight))
    return newImage

image1 = Image.open("images/cores/2ANP-2A_5.548-00-5.550-70_FotoHDI_T01_CX1-3.png")
image2 = Image.open("images/labels/2ANP-2A_5.548-00-5.550-70_FotoHDI_T01_CX1-3.png")

image3 = changeImageSize(338, 9452, image1)
image4 = changeImageSize(338, 9452, image2)

image5 = image3.convert("RGBA")
image6 = image4.convert("RGBA")

alphaBlended1 = Image.blend(image5, image6, alpha=.2)
alphaBlended2 = Image.blend(image5, image6, alpha=.4)
alphaBlended3 = Image.blend(image5, image6, alpha=.4)

