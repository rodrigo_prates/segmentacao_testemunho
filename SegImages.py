import csv
import os
import numpy as np
import pickle
import PIL
import cv2
import math
import tensorflow as tf

#from keras.preprocessing.image import load_img, save_img, ImageDataGenerator
#from keras.applications import MobileNet, MobileNetV2, VGG16, VGG19, Xception, imagenet_utils, ResNet50, ResNet50V2, InceptionV3
from sklearn.metrics import classification_report, confusion_matrix
#from keras.layers.core import Dropout
#from keras.layers.core import Flatten
#from keras.layers.core import Dense
#from keras.layers import Input
#from keras.models import Model
#from keras.optimizers import SGD
#import keras.backend as K

from tensorflow.keras import backend as K
from tensorflow.keras.preprocessing.image import load_img, save_img, ImageDataGenerator
from tensorflow.keras.applications import MobileNet, MobileNetV2, VGG16, VGG19, Xception, imagenet_utils, ResNet50, ResNet50V2, InceptionV3
from tensorflow.keras.layers import Dropout
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Input
from tensorflow.keras.models import Model
from tensorflow.keras.optimizers import SGD

from sklearn.linear_model import LogisticRegression

from keras_segmentation.models.unet import vgg_unet, resnet50_unet, mobilenet_unet
from keras_segmentation.models.segnet import vgg_segnet, segnet, resnet50_segnet, mobilenet_segnet
from keras_segmentation.models.pspnet import pspnet, vgg_pspnet, resnet50_pspnet
from keras_segmentation.models.fcn import fcn_8, fcn_32, fcn_8_vgg, fcn_32_vgg, fcn_8_resnet50, fcn_32_resnet50, fcn_8_mobilenet, fcn_32_mobilenet
from keras_segmentation.predict import predict, predict_multiple, model_from_checkpoint_path

from ImageUtils import ImageUtils
from PostProcessImages import PostProcessImages
from PreProcessImages import PreProcessImages

from datetime import datetime

import gc

from sklearn.model_selection import StratifiedKFold, KFold

import concurrent.futures

from multiprocessing import cpu_count

#import glob

PIL.Image.MAX_IMAGE_PIXELS = 933120000


class SegImages:

   MODELS = {
      "vgg16": VGG16,
      "vgg19": VGG19,
      "inception": InceptionV3,
      "xception": Xception, # TensorFlow ONLY
      "resnet": ResNet50,
      "resnetv2": ResNet50V2,
      "mobilenet": MobileNet,
      "mobilenetv2": MobileNetV2
   }

   SEGMODELS = {
      "vgg_unet": vgg_unet,
      "segnet": segnet,
      "vgg_segnet": vgg_segnet,
      "resnet50_unet": resnet50_unet,
      "mobilenet_unet": mobilenet_unet,
      "resnet50_segnet": resnet50_segnet,
      "mobilenet_segnet": mobilenet_segnet,
      "pspnet": pspnet,
      "vgg_pspnet": vgg_pspnet,
      "resnet50_pspnet": resnet50_pspnet,
      "fcn_8": fcn_8,
      "fcn_32": fcn_32,
      "fcn_8_vgg": fcn_8_vgg,
      "fcn_32_vgg": fcn_32_vgg,
      "fcn_8_resnet50": fcn_8_resnet50,
      "fcn_32_resnet50": fcn_32_resnet50,
      "fcn_8_mobilenet": fcn_8_mobilenet,
      "fcn_32_mobilenet": fcn_32_mobilenet 
   }

   TRAGETSIZE = {
      "vgg16": (224, 224),
      "vgg19": (224, 224),
      "inception": (299, 299),
      "xception": (224, 224), # TensorFlow ONLY
      "resnet": (224, 224),
      "resnetv2": (224, 224),
      "mobilenet": (224, 224),
      "mobilenetv2": (224, 224)
   }

   CLASSIFIERS = {
      "logist_regression": LogisticRegression
   }
	
   def __init__(self, debug, logfolder, logfile, use_gpu, num_gpus):
      self.files = []
      self.debug = True if debug == 1 else False
      self.logfolder = logfolder
      if not os.path.exists(logfolder):
         os.makedirs(logfolder)
      self.logfile = logfile
      self.use_gpu = use_gpu
      self.num_gpus = num_gpus

   def gen_labels_images(self, infolder, labelfolder, binfolder, annfolder, labels, img_extension):
      img_utils = ImageUtils(True, self.logfolder, self.logfile)
      img_utils.write2log(self.logfile, 'start annotations files generation! - {}'.format(datetime.today().strftime('%H:%M:%S')))
      outputfilenames = []

      if not os.path.exists(labelfolder):
         os.makedirs(labelfolder)

      if not os.path.exists(annfolder):
         os.makedirs(annfolder)

      if os.path.exists(infolder):
         img_utils.lower_case_ext(infolder)

      for file in sorted(os.listdir(binfolder)):
         if file.endswith('.bin'):
            try:
               imagefilename = os.path.splitext(file)[0] + '.' + img_extension
               img = load_img(os.path.sep.join([infolder, imagefilename]))
               outputfilename = os.path.splitext(file)[0] + '_bin' + '.' + img_extension
               img_utils.bin2format('segmented_image', os.path.sep.join([binfolder, file]), img.size[1], img.size[0], os.path.sep.join([labelfolder, outputfilename]), labels)
               img_utils.bin2format('annotated_image', os.path.sep.join([binfolder, file]), img.size[1], img.size[0], os.path.sep.join([annfolder, imagefilename]), labels)
               outputfilenames.append(outputfilename)
            except IOError as e:
               img_utils.write2log(self.logfile, str(e) + ' - {}\n'.format(datetime.today().strftime('%H:%M:%S')))
               print(e)

      img_utils.write2log(self.logfile, 'all annotations files generated! - {}\n'.format(datetime.today().strftime('%H:%M:%S')))
      return outputfilenames

   def gen_data(self, coreimagefolder, labelimagefolder, coreoutfolder, labeloutfolder, windowsize, mode, stepsize, 
                offset, img_extension, num_cpus, auto_num_cpus):

      img_utils = ImageUtils(True, self.logfolder, self.logfile)
      img_utils.write2log(self.logfile, 'start windowing files generation! - {}'.format(datetime.today().strftime('%H:%M:%S')))

      if not os.path.exists(coreoutfolder):
         os.makedirs(coreoutfolder)

      if not os.path.exists(labeloutfolder):
         os.makedirs(labeloutfolder)

      files = sorted(os.listdir(coreimagefolder))

      workers = num_cpus if not auto_num_cpus else cpu_count()

      workers = workers if len(files) >= workers else len(files)

      print('number of workers: {}'.format(workers))

      chunk_list = img_utils.chunk_data(files, workers)

      self.img_extension = img_extension
      self.coreimagefolder = coreimagefolder
      self.coreoutfolder = coreoutfolder
      self.windowsize = windowsize
      self.mode = mode
      self.stepsize = stepsize
      self.offset = offset
      self.labelimagefolder = labelimagefolder
      self.labeloutfolder = labeloutfolder

      with concurrent.futures.ProcessPoolExecutor(max_workers=workers) as executor:
         for i in range(0, workers):
            executor.submit(self.windowed_input_label_images, chunk_list[i])

   def windowed_input_label_images(self, input_files):
      img_utils = ImageUtils(True, self.logfolder, self.logfile)
      for file in input_files:
         if file.endswith(self.img_extension):
            try:
               corefilename = os.path.splitext(file)[0] + '.' + self.img_extension
               print("Windowing core image {}".format(corefilename))
               core_img = cv2.imread(os.path.sep.join([self.coreimagefolder, corefilename]))
               self.gen_windowed_images(core_img, os.path.splitext(file)[0], self.coreoutfolder, self.windowsize, 
                                        self.mode, self.stepsize, self.offset, self.img_extension)
               labelfilename = corefilename
               label_img = cv2.imread(os.path.sep.join([self.labelimagefolder, labelfilename]))
               print("Windowing label image {}".format(labelfilename))
               self.gen_windowed_images(label_img, os.path.splitext(file)[0], self.labeloutfolder, self.windowsize, 
                                        self.mode, self.stepsize, self.offset, self.img_extension)
            except IOError as e:
               img_utils.write2log(self.logfile, str(e) + ' - {}\n'.format(datetime.today().strftime('%H:%M:%S')))
               print(e)

      img_utils.write2log(self.logfile, 'all windowing files generated! - {}\n'.format(datetime.today().strftime('%H:%M:%S')))


   """ def gen_data(self, coreimagefolder, labelimagefolder, coreoutfolder, labeloutfolder, windowsize, mode, stepsize, offset, augment, img_extension):
      img_utils = ImageUtils(True, self.logfolder, self.logfile)
      img_utils.write2log(self.logfile, 'start windowing files generation! - {}'.format(datetime.today().strftime('%H:%M:%S')))
      
      augment = True if augment == 1 else False
      if not os.path.exists(coreoutfolder):
         os.makedirs(coreoutfolder)

      if not os.path.exists(labeloutfolder):
         os.makedirs(labeloutfolder)

      core_win_list = []
      label_win_list = []

      for file in sorted(os.listdir(coreimagefolder)):
         if file.endswith(img_extension):
            try:
               corefilename = os.path.splitext(file)[0] + '.' + img_extension
               print("Windowing core image {}".format(corefilename))
               core_img = cv2.imread(os.path.sep.join([coreimagefolder, corefilename]))
               core_win_list = core_win_list + self.gen_windowed_images(core_img, os.path.splitext(file)[0], coreoutfolder, 
                  windowsize, mode, stepsize, offset, augment, img_extension)
            
               labelfilename = corefilename
               label_img = cv2.imread(os.path.sep.join([labelimagefolder, labelfilename]))
               print("Windowing label image {}".format(labelfilename))
               label_win_list = label_win_list + self.gen_windowed_images(label_img, os.path.splitext(file)[0], labeloutfolder, 
                  windowsize, mode, stepsize, offset, augment, img_extension)
            except IOError as e:
               img_utils.write2log(self.logfile, str(e) + ' - {}\n'.format(datetime.today().strftime('%H:%M:%S')))
               print(e)

      img_utils.write2log(self.logfile, 'all windowing files generated! - {}\n'.format(datetime.today().strftime('%H:%M:%S')))
      return core_win_list, label_win_list """

   def gen_windowed_images(self, image, imagename, outfolder, windowsize=[180, 180], mode='horizontal', stepsize=60, offset=2, img_extension='png'):

      height, width = image.shape[:2]
      voffset = round(offset*height/100)
      hoffset = round(offset*width/100)

      win_names = []
      if mode == 'horizontal':
         for vindex in range(voffset, height - windowsize[1] - voffset, stepsize):
            if self.debug:
               newheight = height - windowsize[1] - voffset
               print("Image windowing process: {} %".format(round((100/newheight)*vindex,2)))
            for hindex in range(hoffset, width - windowsize[0] - hoffset, stepsize):
               imgwindow = image[ vindex:vindex + windowsize[1], hindex:hindex + windowsize[0] ]
               win_name = imagename + '_' + str(vindex) + '_' + str(hindex) + '.' + img_extension
               win_names.append(win_name)
               cv2.imwrite(os.path.sep.join([outfolder, win_name]), imgwindow)

      return win_names

   def get_label(self, prof, core_labels):
      numlabels = len(core_labels)
      for row in range(0, len(core_labels)):

         if prof < float(core_labels[row][0]):
            return core_labels[row-1][1]

         if numlabels-1 == row:
            return core_labels[row][1]

   def filterneighbors(self, slidenames_list, slide_train, slide_test, slide_val, label_train, label_val, numneighbors=3):
      for image in slide_test:
         neighbors = ImageUtils.getneighbors(slidenames_list, image, numneighbors)

         slide_train, label_train = ImageUtils.removeimagesfromlist(slide_train, label_train, neighbors)
         slide_val, label_val = ImageUtils.removeimagesfromlist(slide_val, label_val, neighbors)

      return slide_train, slide_val, label_train, label_val

   def validatemodel(self, netmodel):
      if netmodel not in self.MODELS.keys():
         raise AssertionError("The model should be a key in the 'MODELS' dictionary!")

   def validatesegmodel(self, segmodel):
      if segmodel not in self.SEGMODELS.keys():
         raise AssertionError("The segmodel should be a key in the 'SEGMODELS' dictionary!")

   def settrainablelayers(self, basemodel, netmodel):
      if netmodel == 'vgg16':
         for layer in basemodel.layers[15:]:
            layer.trainable = True
      elif netmodel == 'vgg19':
         for layer in basemodel.layers[17:]:
            layer.trainable = True
      elif netmodel == 'inception':
         for layer in basemodel.layers[279:]:
            layer.trainable = True

      return basemodel

   def evalmetrics(self, testlabels, predidxs):
      cm = confusion_matrix(testlabels, predidxs)
      total = sum(sum(cm))
      acc = (cm[0, 0] + cm[1, 1]) / total
      sensitivity = cm[0, 0] / (cm[0, 0] + cm[0, 1])
      specificity = cm[1, 1] / (cm[1, 0] + cm[1, 1])

      print(cm)
      print("acc: {:.4f}".format(acc))
      print("sensitivity: {:.4f}".format(sensitivity))
      print("specificity: {:.4f}".format(specificity))

      return cm, acc, sensitivity, specificity

   def regions_report(self, predictlabels, labels):
      curr_label = labels[0]
      avg_acc = [0]
      avg_idx = 0
      label_count = 0
      for idx in range(1, len(labels)):
         if labels[idx] != curr_label:
            avg_acc[avg_idx] = avg_acc[avg_idx]/label_count
            label_count = 0
            avg_idx += 1
            curr_label = labels[idx]
            avg_acc.append(0)
         else:
            if curr_label == predictlabels[idx]:
               avg_acc[avg_idx] += 1
            label_count += 1

      avg_acc[-1] = avg_acc[-1]/label_count

      return avg_acc

   def feature_extraction(self, trainpath, valpath, testpath, netmodel, autobatchsize, batchsize, meansub, totaltrain, totalval, totaltest, outfolder, label_train, label_val, label_test, labels, sep):
      
      self.validatemodel(netmodel)

      resultfolder = os.path.sep.join([outfolder, netmodel])

      if not os.path.exists(resultfolder):
         os.mkdir(resultfolder)

      if autobatchsize:
         batchsize = ImageUtils.calcbatchsize()

      print("[INFO] batch size {}...".format(str(batchsize)))

      targetsize = self.TRAGETSIZE[netmodel]
      inputsize = (targetsize[0],targetsize[1],3)

      trainaug = ImageDataGenerator()
      valaug = ImageDataGenerator()

      trainaug.mean = meansub
      valaug.mean = meansub

      traingen = trainaug.flow_from_directory(trainpath, 
         class_mode='categorical', target_size=targetsize, color_mode='rgb', shuffle=True, batch_size=batchsize)
      valgen = valaug.flow_from_directory(valpath, 
         class_mode='categorical', target_size=targetsize, color_mode='rgb', shuffle=False, batch_size=batchsize)
      testgen = valaug.flow_from_directory(testpath, 
         class_mode='categorical', target_size=targetsize, color_mode='rgb', shuffle=False, batch_size=batchsize)

      print("[INFO] loading {}...".format(netmodel))
      
      Network = self.MODELS[netmodel]

      basemodel = Network(weights='imagenet', include_top=False, input_tensor=Input(shape=inputsize))

      numlayers = len(basemodel.layers)
      print("[INFO] model number of layers {}...".format(str(numlayers)))

      print("[INFO] model predict for feature extraction...")
      
      train_features = basemodel.predict_generator(traingen, steps=(totaltrain // batchsize) + 1)
      val_features = basemodel.predict_generator(valgen, steps=(totalval // batchsize) + 1)
      test_features = basemodel.predict_generator(testgen, steps=(totaltest // batchsize) + 1)

      train_features = train_features.reshape((train_features.shape[0], 7 * 7 * 512))
      val_features = val_features.reshape((val_features.shape[0], 7 * 7 * 512))
      test_features = test_features.reshape((test_features.shape[0], 7 * 7 * 512))

      labels_map = ImageUtils.encode_labels(labels)

      label_train_encoded = [labels_map[label] for label in label_train]
      label_val_encoded = [labels_map[label] for label in label_val]
      label_test_encoded = [labels_map[label] for label in label_test]

      train_features_path = os.path.sep.join([outfolder, 'trainning_features.csv'])
      self.save_features(train_features_path, sep, label_train_encoded, train_features)

      val_features_path = os.path.sep.join([outfolder, 'validation_features.csv'])
      self.save_features(val_features_path, sep, label_val_encoded, val_features)

      test_features_path = os.path.sep.join([outfolder, 'test_features.csv'])
      self.save_features(test_features_path, sep, label_test_encoded, test_features)

      return train_features_path, val_features_path, test_features_path

   def save_features(self, outfile, sep, label_train, train_features):
      csv = open(outfile, "w")
      for (label, vec) in zip(label_train, train_features):
         vec = sep.join([str(v) for v in vec])
         csv.write("{},{}\n".format(label, vec))
      csv.close()

   # https://github.com/divamgupta/image-segmentation-keras
   def segtrainning(self, inputtrainpath, inputvalpath, outputtrainpath, outputvalpath, segmodel, autobatchsize, batchsize, numlabels, numepochs, 
      outfolder, verify_dataset, modelpath, modelfilepath, input_height=224, input_width=224):

      img_utils = ImageUtils(True, self.logfolder, self.logfile)
      img_utils.write2log(self.logfile, 'start segmentation trainning with folders {}, {}\n model: {}\n, epochs: {}\n - {}'
         .format(inputtrainpath, inputvalpath, segmodel, numepochs, datetime.today().strftime('%H:%M:%S')))

      self.validatesegmodel(segmodel)

      resultfolder = os.path.sep.join([outfolder, segmodel])

      if not os.path.exists(resultfolder):
         os.mkdir(resultfolder)

      if autobatchsize:
         batchsize = img_utils.calcbatchsize()

      print("[INFO] batch size {}...".format(str(batchsize)))

      print("[INFO] loading {}...".format(segmodel))

      print("[INFO] input size {}x{}".format(input_height, input_width))
      
      Network = self.SEGMODELS[segmodel]

      print("[INFO] calculate number of mini-batchs samples...")

      num_samples = len([f for f in os.listdir(inputtrainpath) if os.path.isfile(os.path.join(inputtrainpath, f))])
      steps_per_epoch = math.ceil(num_samples/batchsize)

      num_val_samples = len([f for f in os.listdir(inputvalpath) if os.path.isfile(os.path.join(inputvalpath, f))])
      validation_steps = math.ceil(num_val_samples/batchsize)

      try:
         print('[INFO] loading model {}'.format(segmodel))
         if 'mobilenet' in segmodel:
            K.set_image_data_format('channels_last')

         if self.use_gpu:
            strategy = tf.distribute.MirroredStrategy()
            with strategy.scope():
               model = Network(n_classes=numlabels, input_height=input_height, input_width=input_width)
               model.compile(loss='categorical_crossentropy', optimizer='adadelta' , metrics=['accuracy'])
         else:
            model = Network(n_classes=numlabels, input_height=input_height, input_width=input_width)
            
         print('[INFO] model loaded')

         print('[INFO] START trainning')
         auto_resume = False
         checkpoints_path = os.path.sep.join([resultfolder, segmodel])
         if modelpath or modelfilepath:
            auto_resume = True
            checkpoints_path = modelfilepath if modelfilepath else modelpath
         stories = model.train(train_images=inputtrainpath, train_annotations=outputtrainpath, validate=True, val_images=inputvalpath, 
            val_annotations=outputvalpath, epochs=numepochs, batch_size=batchsize, steps_per_epoch=steps_per_epoch, validation_steps=validation_steps,
            verify_dataset=verify_dataset, with_multi_gpu=self.use_gpu, num_gpus=self.num_gpus, 
            auto_resume_checkpoint=auto_resume, checkpoints_path=checkpoints_path)
         print('[INFO] END trainning')
         print('[INFO] plot trainning')
         postprocess = PostProcessImages(True, self.logfolder, self.logfile)
         history = postprocess.stories2dict(stories)
         plotpath = os.path.sep.join([outfolder, segmodel+'.png'])
         postprocess.plottrainning(history, numepochs, plotpath)

         #Try to clear GPU memory
         del model
         gc.collect()

      except Exception as e:
         img_utils.write2log(self.logfile, str(e) + ' Trainning Error! ' + ' - {}'.format(datetime.today().strftime('%H:%M:%S')))
         print(e)
         print(' Trainning Error! ')

      img_utils.write2log(self.logfile, 'segmentation trainning done! - {}'.format(datetime.today().strftime('%H:%M:%S')))
      return resultfolder

   def segtrainmultiple(self, inputtrainpath, inputvalpath, outputtrainpath, outputvalpath, segmodel_list, autobatchsize, batchsize, numlabels, numepochs, 
      outfolder, verify_dataset, modelpath, modelfilepath, input_height, input_width):
      for model in segmodel_list:
         self.segtrainning(inputtrainpath, inputvalpath, outputtrainpath, outputvalpath, model, autobatchsize, batchsize, numlabels, numepochs, 
            outfolder, verify_dataset, modelpath, modelfilepath, input_height, input_width)

   #https://machinelearningmastery.com/evaluate-performance-deep-learning-models-keras/
   def segcrossvalidtrain(self, inputpath, outputpath, segmodel, autobatchsize, batchsize, numlabels, numepochs, outfolder, verify_dataset, kfolds=10, input_height=224, input_width=224):
      img_utils = ImageUtils(True, self.logfolder, self.logfile)
      img_utils.write2log(self.logfile, 'start cross-validation segmentation trainning with folder {}\n model: {}\n, epochs: {}\n - {}'
         .format(inputpath, segmodel, numepochs, datetime.today().strftime('%H:%M:%S')))

      seed = 7
      np.random.seed(seed)
      Network = self.SEGMODELS[segmodel]
      if autobatchsize:
         batchsize = img_utils.calcbatchsize()

      #kfold = StratifiedKFold(n_splits=kfolds, shuffle=True, random_state=seed)
      kf = KFold(n_splits=kfolds)
      cvscores = []
      count=0
      allInputFiles = np.asarray(sorted(os.listdir(inputpath)))
      allOutputFiles = np.asarray(sorted(os.listdir(outputpath)))
      #print(type(allInputFiles))
      #print(allInputFiles)
      #print("---------------------------------------------------------------------------------------------------------")
      #print(allOutputFiles)
      #for train, val in kfold.split(allInputFiles, allOutputFiles):
      for train_index, val_index in kf.split(allInputFiles):

         #print(len(train_index))
         #print(len(val_index))

         #print(type(val_index))
          
         train = allInputFiles[train_index]
         train_label = allOutputFiles[train_index]

         input_trainpath, output_trainpath = img_utils.organizedataset('trainfold', inputpath, outputpath, outfolder, train, train_label)

         val = allInputFiles[val_index]
         val_label = allOutputFiles[val_index]

         input_valpath, output_valpath = img_utils.organizedataset('testfold', inputpath, outputpath, outfolder, val, val_label)

         model = Network(n_classes=numlabels, input_height=input_height, input_width=input_width)
         stories = model.train(train_images=input_trainpath, train_annotations=output_trainpath, validate=True, val_images=input_valpath, val_annotations=output_valpath, 
         checkpoints_path=os.path.sep.join([resultfolder, segmodel+count]), epochs=numepochs, batch_size=batchsize, verify_dataset=verify_dataset, with_multi_gpu=self.use_gpu, num_gpus=self.num_gpus)

         count+=1
      
         cvscores.append(stories[-1]['val_accuracy']*100)

      print("%.2f%% (+/- %.2f%%)" % (np.mean(cvscores), np.std(cvscores)))
      img_utils.write2log(self.logfile, 'mean: {} and std: {} - {}'.format(np.mean(cvscores), np.std(cvscores), datetime.today().strftime('%H:%M:%S')))
      img_utils.write2log(self.logfile, 'cross-validation segmentation trainning done! - {}'.format(datetime.today().strftime('%H:%M:%S')))   

   def segtest(self, inputpath, modelpath, predfilepath):
      predict(checkpoints_path=modelpath, inp=inputpath, out_fname=predfilepath)

   def segtestmultiple(self, inputspath, modelpath, outfolder):
      predict_multiple(checkpoints_path=modelpath, inp_dir=inputspath, out_dir=outfolder)

   def segeval(self, modelpath, inputtestfile, outputtestfile, windowsize, stepsize, mode, offset, 
      outfolder, modelname, num_labels, label_names, modelfilepath=None, imgNorm="sub_mean", conf_matrix_file_name="confusion_matrix.png"):
      model = model_from_checkpoint_path(modelpath, modelfilepath)
      #print(model.evaluate_segmentation(inp_images_dir=inputtestpath, annotations_dir=outputtestpath))
      img_utils = ImageUtils(True, self.logfolder, self.logfile)
      img_utils.evaluate(model, inputtestfile, outputtestfile, windowsize, stepsize, mode, offset, 
         outfolder, modelname, num_labels, label_names, imgNorm, conf_matrix_file_name)

   def segprediction(self, modelpath, image_folder, annotation_folder, outfolder, modelname, num_labels, label_names, 
   num_cpus, auto_num_cpus, modelfilepath=None, max_images_to_test=100, conf_matrix_file_name="confusion_matrix.png"):
      model = model_from_checkpoint_path(modelpath, modelfilepath)
      img_utils = ImageUtils(True, self.logfolder, self.logfile)
      #img_utils.predict_from_folder(model, image_folder, annotation_folder, 
      #outfolder, modelname, num_labels, label_names, max_images_to_test, conf_matrix_file_name)
      #img_utils.batch_prediction_from_folder(model, image_folder, annotation_folder, outfolder, modelname, 
      #num_labels, label_names, conf_matrix_file_name, max_images_to_test, num_cpus, auto_num_cpus)
      img_utils.gen_prediction_from_folder(model, image_folder, annotation_folder, outfolder, modelname, 
      num_labels, label_names, conf_matrix_file_name)

   def segevalmultiple(self, modelpath, inputspath, outputspath, windowsize, stepsize, mode, offset, outfolder, modelname, num_labels):
      model = model_from_checkpoint_path(modelpath)

      img_utils = ImageUtils(True, self.logfolder, self.logfile)
      img_utils.write2log(self.logfile, 'start test with model {} at {}\n'.format(modelname, datetime.today().strftime('%H:%M:%S')))

      acc_list = []

      for imgfile in sorted(os.listdir(inputspath)):
         #filename = os.path.basename(imgfile)
         imgfilepath = os.path.sep.join([inputspath, imgfile])
         annfilepath = os.path.sep.join([outputspath, imgfile])
         acc = img_utils.evaluate(model, imgfilepath, annfilepath, windowsize, stepsize, mode, offset, outfolder, modelname, num_labels)
         acc_list.append(acc)

      return acc_list


   def segpaintpredictions(self, modelpath, inputspath, labels, windowsize, stepsize, mode, offset, outfolder, modelname):

      #resultfolder = os.path.sep.join([outfolder, modelname])

      #if not os.path.exists(resultfolder):
      #   os.makedirs(resultfolder)

      img_utils = ImageUtils(True, self.logfolder, self.logfile)

      for imgfile in sorted(os.listdir(inputspath)):
         imgfilepath = os.path.sep.join([inputspath, imgfile])
         img_utils.write2log(self.logfile, 'start paint predictions from folder {} - {}'.format(inputspath, datetime.today().strftime('%H:%M:%S')))
         img_utils.paintpredicted(imgfilepath, labels, modelpath, windowsize, stepsize, mode, offset, outfolder, modelname)
         img_utils.write2log(self.logfile, 'End paint predictions from folder {} - {}'.format(inputspath, datetime.today().strftime('%H:%M:%S')))
      

   def trainning(self, trainpath, valpath, testpath, netmodel, autobatchsize, batchsize, meansub, numlabels, totaltrain, totalval, totaltest, numepochs, outfolder, retrain=False):

      self.validatemodel(netmodel)

      resultfolder = os.path.sep.join([outfolder, netmodel])

      if not os.path.exists(resultfolder):
         os.mkdir(resultfolder)

      if autobatchsize:
         batchsize = ImageUtils.calcbatchsize()

      print("[INFO] batch size {}...".format(str(batchsize)))

      targetsize = self.TRAGETSIZE[netmodel]
      inputsize = (targetsize[0],targetsize[1],3)

      trainaug = ImageDataGenerator()
      valaug = ImageDataGenerator()

      trainaug.mean = meansub
      valaug.mean = meansub

      traingen = trainaug.flow_from_directory(trainpath, 
         class_mode='categorical', target_size=targetsize, color_mode='rgb', shuffle=True, batch_size=batchsize)
      valgen = valaug.flow_from_directory(valpath, 
         class_mode='categorical', target_size=targetsize, color_mode='rgb', shuffle=False, batch_size=batchsize)
      testgen = valaug.flow_from_directory(testpath, 
         class_mode='categorical', target_size=targetsize, color_mode='rgb', shuffle=False, batch_size=batchsize)

      print("[INFO] loading {}...".format(netmodel))
      
      Network = self.MODELS[netmodel]

      basemodel = Network(weights='imagenet', include_top=False, input_tensor=Input(shape=inputsize))

      headmodel = basemodel.output
      headmodel = Flatten(name='flatten')(headmodel)
      headmodel = Dense(512, activation='relu')(headmodel)
      headmodel = Dropout(0.5)(headmodel)
      headmodel = Dense(numlabels, activation='softmax')(headmodel)

      model = Model(inputs=basemodel.input, outputs=headmodel)

      numlayers = len(model.layers)
      print("[INFO] model number of layers {}...".format(str(numlayers)))

      for layer in basemodel.layers:
         layer.trainable = False

      for layer in basemodel.layers:
         print('{}: {}'.format(layer, layer.trainable))

      print('[INFO] compiling model...')
      opt = SGD(lr=1e-4, momentum=0.9)
      model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
      
      print('[INFO] trainning head...')
      H = model.fit_generator(traingen, steps_per_epoch=totaltrain // batchsize, 
         validation_data=valgen, validation_steps=totalval // batchsize, epochs=numepochs)

      print("[INFO] evaluating after fine-tuning network head...")
      testgen.reset()
      predidxs = model.predict_generator(testgen, steps=(totaltest // batchsize) + 1)
      predidxs = np.argmax(predidxs, axis=1)

      print(classification_report(testgen.classes, predidxs, target_names=testgen.class_indices.keys()))
      headtrainpath = os.path.sep.join([resultfolder, 'headtrainning.png'])
      PostProcessImages.plottrainning(H, numepochs, headtrainpath)

      self.evalmetrics(testgen.classes, predidxs)

      if retrain:
         traingen.reset()
         valgen.reset()

         basemodel = self.settrainablelayers(basemodel, netmodel)

         for layer in basemodel.layers:
            print('{}: {}'.format(layer, layer.trainable))

         print("[INFO] re-compiling model...")
         opt = SGD(lr=1e-4, momentum=0.9)
         model.compile(loss="categorical_crossentropy", optimizer=opt, metrics=["accuracy"])
         H = model.fit_generator(traingen, steps_per_epoch=totaltrain // batchsize, 
            validation_data=valgen, validation_steps=totalval // batchsize, epochs=numepochs)

         print("[INFO] evaluating after fine-tuning network...")
         testgen.reset()
         predidxs = model.predict_generator(testgen, steps=(totaltest // batchsize) + 1)
         predidxs = np.argmax(predidxs, axis=1)
         print(classification_report(testgen.classes, predidxs, target_names=testgen.class_indices.keys()))
         trainpath = os.path.sep.join([resultfolder, 'trainning.png'])
         PostProcessImages.plottrainning(H, numepochs, trainpath)

         self.evalmetrics(testgen.classes, predidxs)
 
      # serialize the model to disk
      print("[INFO] serializing network...")
      modelpath = os.path.sep.join([resultfolder, 'core_net.model'])
      model.save(modelpath)

      return modelpath

   def train_from_features(self, train_features_path, val_features_path, test_features_path, labels, outfolder, classifier):

      resultfolder = os.path.sep.join([outfolder, classifier])

      if not os.path.exists(resultfolder):
         os.mkdir(resultfolder)
      
      print("[INFO] loading data...")
      (trainX, trainY) = ImageUtils.load_features(train_features_path)
      (valX, valY) = ImageUtils.load_features(val_features_path)
      (testX, testY) = ImageUtils.load_features(test_features_path)

      print("[INFO] training model...")
      CLF = self.CLASSIFIERS[classifier]
      model = CLF(solver="lbfgs", multi_class="auto", max_iter=1000)
      model.fit(trainX, trainY)

      print("[INFO] evaluating...")
      preds = model.predict(testX)
      print(classification_report(testY, preds, target_names=labels))

      print("[INFO] saving model...")
      f = open(os.path.sep.join([resultfolder, 'model.cpickle']), "wb")
      f.write(pickle.dumps(model))
      f.close()
