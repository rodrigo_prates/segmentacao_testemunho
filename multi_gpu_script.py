import math
import json
import keras.backend as K
from keras_segmentation.models.unet import vgg_unet, resnet50_unet, mobilenet_unet
from keras_segmentation.data_utils.data_loader import image_segmentation_generator
import config
import tensorflow as tf
import os
from keras.utils import multi_gpu_model
from keras.preprocessing.image import load_img, save_img, ImageDataGenerator
input_trainpath = os.path.sep.join([config.OUTPUT_FOLDER, 'trainning_input'])
output_trainpath = os.path.sep.join([config.OUTPUT_FOLDER, 'trainning_output'])
input_valpath = os.path.sep.join([config.OUTPUT_FOLDER, 'validation_input'])
output_valpath = os.path.sep.join([config.OUTPUT_FOLDER, 'validation_output'])
input_testpath = os.path.sep.join([config.OUTPUT_FOLDER, 'testing_input'])
output_testpath = os.path.sep.join([config.OUTPUT_FOLDER, 'testing_output'])
K.set_image_data_format('channels_last')
batch_size=32
#with tf.device('/cpu:0'):
#   model = mobilenet_unet(n_classes=18, input_height=224, input_width=224)
model = mobilenet_unet(n_classes=18, input_height=224, input_width=224)
n_classes = model.n_classes
input_height = model.input_height
input_width = model.input_width
output_height = model.output_height
output_width = model.output_width
model_name = model.model_name
train_gen = image_segmentation_generator( input_trainpath,output_trainpath,batch_size,n_classes,input_width,input_height,output_height,output_width)
#val_gen  = image_segmentation_generator( input_valpath,output_valpath,2,n_classes,input_height,input_width,output_height, output_width)
num_samples = len([f for f in os.listdir(input_trainpath) if os.path.isfile(os.path.join(input_trainpath, f))])
steps_per_epoch = math.ceil(num_samples/batch_size)
resultfolder = os.path.sep.join([config.OUTPUT_FOLDER, model_name])
checkpoints_path = os.path.sep.join([resultfolder, model_name])
#parallel_model = multi_gpu_model(model, gpus=4)
parallel_model = multi_gpu_model(model, cpu_relocation=True)
parallel_model.compile(loss='categorical_crossentropy', optimizer='adadelta' , metrics=['accuracy'])
#history = parallel_model.fit_generator( train_gen , steps_per_epoch, epochs=2, verbose=1, validation_data = val_gen, validation_steps=200)
parallel_model.fit_generator( train_gen , steps_per_epoch  , epochs=2, verbose=1 )

#watch -d -n 0.5 nvidia-smi


#testaug = ImageDataGenerator()
#trainaug.mean = meansub
#testgen = testaug.flow_from_directory(input_testpath, class_mode='categorical', target_size=(224,224), color_mode='rgb', shuffle=True, batch_size=batch_size)

#fit()?????


import math
import json
import tensorflow as tf
import config
import os
from tensorflow.keras import backend as K
from keras_segmentation.models.unet import vgg_unet, resnet50_unet, mobilenet_unet
from keras_segmentation.data_utils.data_loader import image_segmentation_generator
from tensorflow.keras.preprocessing.image import load_img, save_img, ImageDataGenerator


strategy = tf.distribute.MirroredStrategy()
input_trainpath = os.path.sep.join([config.OUTPUT_FOLDER, 'trainning_input'])
output_trainpath = os.path.sep.join([config.OUTPUT_FOLDER, 'trainning_output'])
input_valpath = os.path.sep.join([config.OUTPUT_FOLDER, 'validation_input'])
output_valpath = os.path.sep.join([config.OUTPUT_FOLDER, 'validation_output'])
input_testpath = os.path.sep.join([config.OUTPUT_FOLDER, 'testing_input'])
output_testpath = os.path.sep.join([config.OUTPUT_FOLDER, 'testing_output'])
K.set_image_data_format('channels_last')
batch_size=256

with strategy.scope():
	model = mobilenet_unet(n_classes=18, input_height=224, input_width=224)
	model.compile(loss='categorical_crossentropy', optimizer='adadelta' , metrics=['accuracy'])

n_classes = model.n_classes
input_height = model.input_height
input_width = model.input_width
output_height = model.output_height
output_width = model.output_width
model_name = model.model_name
train_gen = image_segmentation_generator( input_trainpath,output_trainpath,batch_size,n_classes,input_width,input_height,output_height,output_width)

num_samples = len([f for f in os.listdir(input_trainpath) if os.path.isfile(os.path.join(input_trainpath, f))])
steps_per_epoch = math.ceil(num_samples/batch_size)
resultfolder = os.path.sep.join([config.OUTPUT_FOLDER, model_name])
checkpoints_path = os.path.sep.join([resultfolder, model_name])

model.fit(train_gen,epochs=2,steps_per_epoch=steps_per_epoch)