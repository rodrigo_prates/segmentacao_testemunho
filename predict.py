import os
import config

os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
if config.USE_GPU:
   if config.GPU_ID:
      os.environ["CUDA_VISIBLE_DEVICES"] = config.GPU_ID
else:
   os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

from SegImages import SegImages
from PostProcessImages import PostProcessImages
from PreProcessImages import PreProcessImages
from ImageUtils import ImageUtils
from datetime import datetime

from keras import backend as K
import time


INPUT_FOLDER = config.INPUT_IMAGES_FOLDER
LABEL_FOLDER = config.LABEL_IMAGES_FOLDER
BIN_FOLDER = config.BIN_IMAGES_FOLDER
ANN_FOLDER = config.ANN_IMAGES_FOLDER
OUTPUT_FOLDER = config.OUTPUT_FOLDER
WINDOWS_SIZE = [config.WINDOW_WIDTH, config.WINDOW_HEIGHT]
WINDOW_MODE = config.WINDOW_MODE
WINDOW_STEP = config.WINDOW_STEP
WINDOW_OFFSET = config.WINDOW_OFFSET
AUGMENT = config.AUGMENT

IMAGE_EXTENSION = config.IMAGE_EXTENSION
DEBUG = config.DEBUG
TRAINSIZE = config.TRAINSIZE

OUTPUT_CORE_FOLDER = config.OUTPUT_CORE_FOLDER
OUTPUT_LABEL_FOLDER = config.OUTPUT_LABEL_FOLDER
OUTPUT_PREDICTION_FOLDER = config.OUTPUT_PREDICTION_FOLDER

BATCH_SIZE=config.BATCH_SIZE
IMAGENET_MEAN = config.IMAGENET_MEAN
NUM_LABELS=config.NUM_LABELS
AUTO_BATCH_SIZE = config.AUTO_BATCH_SIZE
NETMODEL = config.NETMODEL
SEGMODEL = config.SEGMODEL
RETRAIN = config.RETRAIN
NUMEPOCHS = config.NUMEPOCHS

LABELS = config.LABELS
NUM_LABELS = config.NUM_LABELS
LABEL_NAMES = config.LABEL_NAMES

LOGS_FOLDER = config.LOGS_FOLDER
logfile = os.path.sep.join([LOGS_FOLDER, 'logs_' + datetime.today().strftime('%Y_%m_%d') + '.txt'])

NUM_GPUS = config.NUM_GPUS
USE_GPU = config.USE_GPU

NUM_CPUS = config.NUM_CPUS
AUTO_NUM_CPUS = config.AUTO_NUM_CPUS

segimages = SegImages(DEBUG, LOGS_FOLDER, logfile, USE_GPU, NUM_GPUS)
img_utils = ImageUtils(DEBUG, LOGS_FOLDER, logfile)

#imgfile = 'images/cores/2ANP-2A_5.548-00-5.550-70_FotoHDI_T01_CX1-3.png'
#annfile = 'images/annotated/2ANP-2A_5.548-00-5.550-70_FotoHDI_T01_CX1-3.png'
image_folder = os.path.sep.join([OUTPUT_FOLDER, 'testing_input']) #'/home/petrec/Downloads/segmentacao_testemunho/output/testing_input'
annotation_folder = os.path.sep.join([OUTPUT_FOLDER, 'testing_output'])
modelname = 'mobilenet_unet'
modelpath = 'output/' + modelname + '/' + modelname
#modelfilepath = os.path.sep.join([OUTPUT_FOLDER, modelname, 'mobilenet_segnet.18'])
modelfilepath = None
confusion_matrix = "confusion_matrix.png"

NUM_IMAGES_TO_TEST = 500
#segimages.segeval(modelpath, imgfile, annfile, WINDOWS_SIZE, WINDOW_STEP, WINDOW_MODE, WINDOW_OFFSET, OUTPUT_PREDICTION_FOLDER, modelname, NUM_LABELS, modelfilepath)
if 'mobilenet' in modelname:
   K.set_image_data_format('channels_last')

start = time.time()
segimages.segprediction(modelpath, image_folder, annotation_folder, OUTPUT_PREDICTION_FOLDER, modelname, 
NUM_LABELS, LABEL_NAMES, NUM_CPUS, AUTO_NUM_CPUS, modelfilepath, NUM_IMAGES_TO_TEST, confusion_matrix)
print("it took", time.time() - start, "seconds.")

#img_utils.check_dataset_balance(LABELS, ANN_FOLDER)
