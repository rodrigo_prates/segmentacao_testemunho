import os
import config

os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
if config.USE_GPU:
   if config.GPU_ID:
      os.environ["CUDA_VISIBLE_DEVICES"] = config.GPU_ID
else:
   os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

from SegImages import SegImages
from PostProcessImages import PostProcessImages
from PreProcessImages import PreProcessImages
from ImageUtils import ImageUtils
from datetime import datetime

from keras import backend as K
import time


INPUT_FOLDER = config.INPUT_IMAGES_FOLDER
LABEL_FOLDER = config.LABEL_IMAGES_FOLDER
BIN_FOLDER = config.BIN_IMAGES_FOLDER
ANN_FOLDER = config.ANN_IMAGES_FOLDER
OUTPUT_FOLDER = config.OUTPUT_FOLDER
WINDOWS_SIZE = [config.WINDOW_WIDTH, config.WINDOW_HEIGHT]
WINDOW_MODE = config.WINDOW_MODE
WINDOW_STEP = config.WINDOW_STEP
WINDOW_OFFSET = config.WINDOW_OFFSET
AUGMENT = config.AUGMENT

IMAGE_EXTENSION = config.IMAGE_EXTENSION
DEBUG = config.DEBUG
TRAINSIZE = config.TRAINSIZE

OUTPUT_CORE_FOLDER = config.OUTPUT_CORE_FOLDER
OUTPUT_LABEL_FOLDER = config.OUTPUT_LABEL_FOLDER
OUTPUT_PREDICTION_FOLDER = config.OUTPUT_PREDICTION_FOLDER

BATCH_SIZE=config.BATCH_SIZE
IMAGENET_MEAN = config.IMAGENET_MEAN
NUM_LABELS=config.NUM_LABELS
AUTO_BATCH_SIZE = config.AUTO_BATCH_SIZE
NETMODEL = config.NETMODEL
SEGMODEL = config.SEGMODEL
RETRAIN = config.RETRAIN
NUMEPOCHS = config.NUMEPOCHS

LABELS = config.LABELS
NUM_LABELS = config.NUM_LABELS
LABEL_NAMES = config.LABEL_NAMES

LOGS_FOLDER = config.LOGS_FOLDER
logfile = os.path.sep.join([LOGS_FOLDER, 'logs_' + datetime.today().strftime('%Y_%m_%d') + '.txt'])

NUM_GPUS = config.NUM_GPUS
USE_GPU = config.USE_GPU

segimages = SegImages(DEBUG, LOGS_FOLDER, logfile, USE_GPU, NUM_GPUS)
img_utils = ImageUtils(DEBUG, LOGS_FOLDER, logfile)

#imgfile = 'images/cores/2ANP-2A_5.548-00-5.550-70_FotoHDI_T01_CX1-3.png'
#annfile = 'images/annotated/2ANP-2A_5.548-00-5.550-70_FotoHDI_T01_CX1-3.png'
#Total  IoU  0.9793535468809403 mobilenet_unet 9
#Std Iou 0.0757234712727993

#Total  IoU  0.8855479511700044 mobilenet_unet 4
#Std Iou 0.24554532546941496

#imgfile = 'images/cores/3-RJS-749D-RJ_5.374-60-5.376-55_FotoHDI_T2_CX19-21.png'
#annfile = 'images/annotated/3-RJS-749D-RJ_5.374-60-5.376-55_FotoHDI_T2_CX19-21.png'
#Total  IoU  0.9625217735948522 mobilenet_unet 9
#Std Iou 0.11331109527326252

#imgfile = 'images/cores/2ANP-2A_5.550-70-5.553-40_FotoHDI_T01_CX4-6.png'
#annfile = 'images/annotated/2ANP-2A_5.550-70-5.553-40_FotoHDI_T01_CX4-6.png'
# just 5 rocks in training
# Total  IoU  0.9779004376519275
# Std Iou 0.07651584908434335
# Total IoU for each class [0.         0.         0.         0.         0.         0.
#  0.         0.         0.         0.         0.         0.98295341
#  0.98181562 0.         0.         0.         0.         0.91474072]
# Std IoU for each class [0.         0.         0.         0.         0.         0.
#  0.         0.         0.         0.         0.         0.07951201
#  0.08496214 0.         0.         0.         0.         0.20638558]

#imgfile = 'images/cores/2ANP-2A_5.548-00-5.550-70_FotoHDI_T01_CX1-3.png'
#annfile = 'images/annotated/2ANP-2A_5.548-00-5.550-70_FotoHDI_T01_CX1-3.png'
# just 5 rocks in training. This is a totally new rock image to predict
# Total  IoU  0.5189639567975884
# Std Iou 0.4184374413603592
# Total IoU for each class [0.         0.         0.         0.         0.         0.
#  0.         0.         0.         0.         0.         0.5375361
#  0.22475344 0.         0.         0.         0.         0.66169478]
# Std IoU for each class [0.         0.         0.         0.         0.         0.
#  0.         0.         0.         0.         0.         0.43362627
#  0.32553166 0.         0.         0.         0.         0.37537864]

#imgfile = 'images/cores/2ANP-2A_5.613-00-5.615-65_FotoHDI_T02_CX1-3.png'
#annfile = 'images/annotated/2ANP-2A_5.613-00-5.615-65_FotoHDI_T02_CX1-3.png'
# just 5 rocks in training. This is another totally new rock image for prediction
# Total  IoU  0.43936332133347206
# Std Iou 0.39747847150872934
# Total IoU for each class [0.         0.         0.         0.         0.         0.
#  0.         0.         0.         0.         0.         0.
#  0.         0.26045522 0.         0.         0.         0.67590932]
# Std IoU for each class [0.         0.         0.         0.         0.         0.
#  0.         0.         0.         0.         0.         0.
#  0.         0.35151604 0.         0.         0.         0.34900088]

# imgfile = 'images/cores/3-RJS-749D-RJ_5.371-90-5.374-60_FotoHDI_T2_CX16-18.png'
# annfile = 'images/annotated/3-RJS-749D-RJ_5.371-90-5.374-60_FotoHDI_T2_CX16-18.png'
# just 5 rocks in training. This is another totally new rock image for prediction
# Total  IoU  0.46303222991652765
# Std Iou 0.39890090678415197
# Total IoU for each class [0.         0.         0.         0.         0.         0.
#  0.         0.62506294 0.         0.         0.         0.
#  0.         0.         0.         0.         0.         0.33496742]
# Std IoU for each class [0.         0.         0.         0.         0.         0.
#  0.         0.37717013 0.         0.         0.         0.
#  0.         0.         0.         0.         0.         0.37227108]

# imgfile = 'images/cores/3-RJS-749D-RJ_5.371-90-5.374-60_FotoHDI_T2_CX16-18.png'
# annfile = 'images/annotated/3-RJS-749D-RJ_5.371-90-5.374-60_FotoHDI_T2_CX16-18.png'
# 5 + 2 rocks in training. This is another totally new rock image for prediction
# Total  IoU  0.48656509093532796
# Std Iou 0.4292982424431209
# Total IoU for each class [0.         0.         0.         0.         0.         0.
#  0.         0.74434685 0.         0.         0.         0.
#  0.         0.         0.         0.         0.         0.27291024]
# Std IoU for each class [0.         0.         0.         0.         0.         0.
#  0.         0.33445475 0.         0.         0.         0.
#  0.         0.         0.         0.         0.         0.37995324]

# imgfile = 'images/cores/3-RJS-749D-RJ_5.371-90-5.374-60_FotoHDI_T2_CX16-18.png'
# annfile = 'images/annotated/3-RJS-749D-RJ_5.371-90-5.374-60_FotoHDI_T2_CX16-18.png'
# 5 + 2 rocks in training with more 5 epochs. This is another totally new rock image for prediction
# Total  IoU  0.542552247527398
# Std Iou 0.42253765131794524
# Total IoU for each class [0.         0.         0.         0.         0.         0.
#  0.         0.8169178  0.         0.         0.         0.
#  0.         0.         0.         0.         0.         0.30219509]
# Std IoU for each class [0.         0.         0.         0.         0.         0.
#  0.         0.27729347 0.         0.         0.         0.
#  0.         0.         0.         0.         0.         0.37960165]

# imgfile = 'images/cores/2ANP-2A_5.548-00-5.550-70_FotoHDI_T01_CX1-3.png'
# annfile = 'images/annotated/2ANP-2A_5.548-00-5.550-70_FotoHDI_T01_CX1-3.png'
# 5 + 2 rocks in training with more 5 epochs. This is another totally new rock image for prediction
# Total  IoU  0.185967343950641
# Std Iou 0.33979943546013286
# Total IoU for each class [0.         0.         0.         0.         0.         0.
#  0.         0.         0.         0.         0.         0.
#  0.         0.         0.         0.         0.         0.60283096]
# Std IoU for each class [0.        0.        0.        0.        0.        0.        0.
#  0.        0.        0.        0.        0.        0.        0.
#  0.        0.        0.        0.3779543]

modelname = 'mobilenet_unet'
modelpath = 'output/' + modelname + '/' + modelname
#modelfilepath = os.path.sep.join([OUTPUT_FOLDER, modelname, 'mobilenet_unet.4'])
#WINDOWS_SIZE = [2*config.WINDOW_WIDTH, 2*config.WINDOW_HEIGHT]
#imgNorm = "sub_mean_no_resize"
#WINDOW_STEP = config.WINDOW_STEP
if 'mobilenet' in modelname:
   K.set_image_data_format('channels_last')

start = time.time()
confusion_matrix = "confusion_matrix_3rjs_t2_cx16-18.png"
segimages.segeval(modelpath, imgfile, annfile, WINDOWS_SIZE, WINDOW_STEP, WINDOW_MODE, WINDOW_OFFSET, 
	OUTPUT_PREDICTION_FOLDER, modelname, NUM_LABELS, LABEL_NAMES, modelfilepath, imgNorm, confusion_matrix)
print("it took", time.time() - start, "seconds.")

#img_utils.check_dataset_balance(LABELS, ANN_FOLDER)
