import os
import config

os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
if config.USE_GPU:
   if config.GPU_ID:
      os.environ["CUDA_VISIBLE_DEVICES"] = config.GPU_ID
else:
   os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

from SegImages import SegImages
from PostProcessImages import PostProcessImages
from PreProcessImages import PreProcessImages
from ImageUtils import ImageUtils
from datetime import datetime

from statistics import mean

from datetime import datetime


INPUT_FOLDER = config.INPUT_IMAGES_FOLDER
LABEL_FOLDER = config.LABEL_IMAGES_FOLDER
BIN_FOLDER = config.BIN_IMAGES_FOLDER
ANN_FOLDER = config.ANN_IMAGES_FOLDER
OUTPUT_FOLDER = config.OUTPUT_FOLDER
WINDOWS_SIZE = [config.WINDOW_WIDTH, config.WINDOW_HEIGHT]
WINDOW_MODE = config.WINDOW_MODE
WINDOW_STEP = config.WINDOW_STEP
WINDOW_OFFSET = config.WINDOW_OFFSET
AUGMENT = config.AUGMENT

IMAGE_EXTENSION = config.IMAGE_EXTENSION
DEBUG = config.DEBUG
TRAINSIZE = config.TRAINSIZE

OUTPUT_CORE_FOLDER = config.OUTPUT_CORE_FOLDER
OUTPUT_LABEL_FOLDER = config.OUTPUT_LABEL_FOLDER
OUTPUT_PREDICTION_FOLDER = config.OUTPUT_PREDICTION_FOLDER

BATCH_SIZE=config.BATCH_SIZE
IMAGENET_MEAN = config.IMAGENET_MEAN
NUM_LABELS=config.NUM_LABELS
AUTO_BATCH_SIZE = config.AUTO_BATCH_SIZE
NETMODEL = config.NETMODEL
SEGMODEL = config.SEGMODEL
RETRAIN = config.RETRAIN
NUMEPOCHS = config.NUMEPOCHS

LABELS = config.LABELS
NUM_LABELS = config.NUM_LABELS

LOGS_FOLDER = config.LOGS_FOLDER
logfile = os.path.sep.join([LOGS_FOLDER, 'logs_' + datetime.today().strftime('%Y_%m_%d') + '.txt'])

NUM_GPUS = config.NUM_GPUS
USE_GPU = config.USE_GPU

segimages = SegImages(DEBUG, LOGS_FOLDER, logfile, USE_GPU, NUM_GPUS)
img_utils = ImageUtils(DEBUG, LOGS_FOLDER, logfile)
post_process = PostProcessImages(DEBUG, LOGS_FOLDER, logfile)

#imgfile = 'images/cores/2ANP-2A_5.548-00-5.550-70_FotoHDI_T01_CX1-3.png'
#annfile = 'images/annotated/2ANP-2A_5.548-00-5.550-70_FotoHDI_T01_CX1-3.png'

#segimages.segeval(modelpath, imgfile, annfile, WINDOWS_SIZE, WINDOW_STEP, WINDOW_MODE, WINDOW_OFFSET, OUTPUT_PREDICTION_FOLDER, modelname) #modelpath, inputtestfile, outputtestfile, windowsize, stepsize, mode, offset

#segimages.segevalmultiple(modelpath, INPUT_FOLDER, ANN_FOLDER, WINDOWS_SIZE, WINDOW_STEP, WINDOW_MODE, WINDOW_OFFSET, OUTPUT_PREDICTION_FOLDER, modelname)

#img_utils.check_dataset_balance(LABELS, ANN_FOLDER)

# SEGMODELS = ["vgg_unet", 
#              "resnet50_unet", 
#              "mobilenet_unet", 
#              "vgg_segnet", 
#              "segnet", 
#              "resnet50_segnet", 
#              "mobilenet_segnet", 
#              "pspnet", 
#              "vgg_pspnet", 
#              "resnet50_pspnet", 
#              "fcn_8", 
#              "fcn_32", 
#              "fcn_8_vgg", 
#              "fcn_32_vgg", 
#              "fcn_8_resnet50", 
#              "fcn_32_resnet50", 
#              "fcn_8_mobilenet", 
#              "fcn_32_mobilenet"]

SEGMODELS = ["vgg_unet", 
             "resnet50_unet", 
             "mobilenet_unet", 
             "vgg_segnet", 
             "segnet", 
             "resnet50_segnet", 
             "mobilenet_segnet"]

models_acc = []

for model in SEGMODELS:
   modelpath = 'output/' + model + '/' + model
   #segimages.segpaintpredictions(modelpath, INPUT_FOLDER, LABELS, WINDOWS_SIZE, config.WINDOW_WIDTH, WINDOW_MODE, WINDOW_OFFSET, OUTPUT_PREDICTION_FOLDER, model)
   image_accuracies = segimages.segevalmultiple(modelpath, INPUT_FOLDER, ANN_FOLDER, WINDOWS_SIZE, config.WINDOW_WIDTH, WINDOW_MODE, WINDOW_OFFSET, OUTPUT_PREDICTION_FOLDER, model, NUM_LABELS)
   models_acc.append(mean(image_accuracies))

print("Mean Models Accuracies for All Images")
print(models_acc) #[0.17216774414109895, 0.1577627359811292, 0.1742236180624902, 0.15255750972310353, 0.16868622000431874, 0.2001878537735979, 0.16565115263160582]

try:
   plotpath = os.path.sep.join([OUTPUT_PREDICTION_FOLDER, 'models_prediction.png'])
   post_process.plotacc(models_acc, plotpath, SEGMODELS)
except Exception as e:
   print(e)
   print('Error plot accuracies')
#melhores resultados atuais: mobilenet_unet, vgg_unet, vgg_segnet

# modelpath1 = 'output/vgg_unet/vgg_unet'
# modelname1 = 'vgg_unet'
# segimages.segpaintpredictions(modelpath1, INPUT_FOLDER, LABELS, WINDOWS_SIZE, config.WINDOW_WIDTH, WINDOW_MODE, WINDOW_OFFSET, OUTPUT_PREDICTION_FOLDER, modelname1)
# segimages.segevalmultiple(modelpath1, INPUT_FOLDER, ANN_FOLDER, WINDOWS_SIZE, config.WINDOW_WIDTH, WINDOW_MODE, WINDOW_OFFSET, OUTPUT_PREDICTION_FOLDER, modelname1)

# modelpath2 = 'output/segnet/segnet'
# modelname2 = 'segnet'
# segimages.segpaintpredictions(modelpath2, INPUT_FOLDER, LABELS, WINDOWS_SIZE, config.WINDOW_WIDTH, WINDOW_MODE, WINDOW_OFFSET, OUTPUT_PREDICTION_FOLDER, modelname2)
# segimages.segevalmultiple(modelpath2, INPUT_FOLDER, ANN_FOLDER, WINDOWS_SIZE, config.WINDOW_WIDTH, WINDOW_MODE, WINDOW_OFFSET, OUTPUT_PREDICTION_FOLDER, modelname2)

# modelpath3 = 'output/vgg_segnet/vgg_segnet'
# modelname3 = 'vgg_segnet'
# segimages.segpaintpredictions(modelpath3, INPUT_FOLDER, LABELS, WINDOWS_SIZE, config.WINDOW_WIDTH, WINDOW_MODE, WINDOW_OFFSET, OUTPUT_PREDICTION_FOLDER, modelname3)
# segimages.segevalmultiple(modelpath3, INPUT_FOLDER, ANN_FOLDER, WINDOWS_SIZE, config.WINDOW_WIDTH, WINDOW_MODE, WINDOW_OFFSET, OUTPUT_PREDICTION_FOLDER, modelname3)
