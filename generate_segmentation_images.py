from SegImages import SegImages
import os

os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

seg = SegImages()
DIRPATH = os.getcwd()
inputpath = os.path.sep.join([DIRPATH, 'MIAS_PNG', 'MIAS', 'images'])
labelpath = os.path.sep.join([DIRPATH, 'MIAS_PNG', 'MIAS', 'labels'])
outputpath = os.path.sep.join([DIRPATH, 'vgg16'])
#seg.split_data_from_folder(inputpath, labelpath, outputpath)

inputtrainpath = os.path.sep.join([outputpath, 'train_input', 'train'])
inputvalpath = os.path.sep.join([outputpath, 'val_input', 'val'])
outputtrainpath = os.path.sep.join([outputpath, 'train_output', 'train'])
outputvalpath = os.path.sep.join([outputpath, 'val_output', 'val'])

seg.seg_trainning(inputtrainpath, inputvalpath, outputtrainpath, outputvalpath)